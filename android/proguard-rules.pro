


## pangle 穿山甲原有的
-keep class com.bytedance.sdk.openadsdk.** { *; }
-keep public interface com.bytedance.sdk.openadsdk.downloadnew.** {*;}
-keep class com.pgl.sys.ces.** {*;}
-keep class com.bytedance.embed_dr.** {*;}
-keep class com.bytedance.embedapplog.** {*;}

## pangle 插件新增 穿山甲插件化版本新增
-keep public class com.ss.android.**{*;}
-keeppackagenames com.bytedance.sdk.openadsdk.api
-keeppackagenames com.bytedance.embed_dr
-keeppackagenames com.bytedance.embedapplog
-keeppackagenames com.ss.android

## 聚合混淆
-keep class bykvm*.**
-keep class com.bytedance.msdk.adapter.**{ public *; }
-keep class com.bytedance.msdk.api.** {
 public *;
}
#-keep class com.bytedance.msdk.base.TTBaseAd{*;}
#-keep class com.bytedance.msdk.adapter.TTAbsAdLoaderAdapter{
#    public *;
#    protected <fields>;
#}

#oaid 不同的版本混淆代码不太一致，你注意你接入的oaid版本
-dontwarn com.bun.**
-keep class com.bun.** {*;}
-keep class a.**{*;}
-keep class XI.CA.XI.**{*;}
-keep class XI.K0.XI.**{*;}
-keep class XI.XI.K0.**{*;}
-keep class XI.vs.K0.**{*;}
-keep class XI.xo.XI.XI.**{*;}
-keep class com.asus.msa.SupplementaryDID.**{*;}
-keep class com.asus.msa.sdid.**{*;}
-keep class com.huawei.hms.ads.identifier.**{*;}
-keep class com.samsung.android.deviceidservice.**{*;}
-keep class com.zui.opendeviceidlibrary.**{*;}
-keep class org.json.**{*;}
-keep public class com.netease.nis.sdkwrapper.Utils {public <methods>;}

