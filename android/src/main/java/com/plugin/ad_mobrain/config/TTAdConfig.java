package com.plugin.ad_mobrain.config;

public class TTAdConfig {
    private String ADD_ID;
    private String APP_NAME;
    private Boolean OPEN_DEBUG;
    private Boolean USE_PANGLE_TEXT_URE_VIEW;
    private Boolean ALLOW_PANGLE_SHOW_NOTIFY;

    public String getADD_ID() {
        return ADD_ID;
    }

    public void setADD_ID(String ADD_ID) {
        this.ADD_ID = ADD_ID;
    }

    public String getAPP_NAME() {
        return APP_NAME;
    }

    public void setAPP_NAME(String APP_NAME) {
        this.APP_NAME = APP_NAME;
    }

    public Boolean getOPEN_DEBUG() {
        return OPEN_DEBUG;
    }

    public void setOPEN_DEBUG(Boolean OPEN_DEBUG) {
        this.OPEN_DEBUG = OPEN_DEBUG;
    }

    public Boolean getUSE_PANGLE_TEXT_URE_VIEW() {
        return USE_PANGLE_TEXT_URE_VIEW;
    }

    public void setUSE_PANGLE_TEXT_URE_VIEW(Boolean USE_PANGLE_TEXT_URE_VIEW) {
        this.USE_PANGLE_TEXT_URE_VIEW = USE_PANGLE_TEXT_URE_VIEW;
    }

    public Boolean getALLOW_PANGLE_SHOW_NOTIFY() {
        return ALLOW_PANGLE_SHOW_NOTIFY;
    }

    public void setALLOW_PANGLE_SHOW_NOTIFY(Boolean ALLOW_PANGLE_SHOW_NOTIFY) {
        this.ALLOW_PANGLE_SHOW_NOTIFY = ALLOW_PANGLE_SHOW_NOTIFY;
    }

    @Override
    public String toString() {
        return "TTAdConfig{" +
                "ADD_ID='" + ADD_ID + '\'' +
                ", APP_NAME='" + APP_NAME + '\'' +
                ", OPEN_DEBUG=" + OPEN_DEBUG +
                ", USE_PANGLE_TEXT_URE_VIEW=" + USE_PANGLE_TEXT_URE_VIEW +
                ", ALLOW_PANGLE_SHOW_NOTIFY=" + ALLOW_PANGLE_SHOW_NOTIFY +
                '}';
    }
}
