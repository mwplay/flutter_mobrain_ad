package com.plugin.ad_mobrain.config;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;

import com.bytedance.msdk.api.TTAdConfig;
import com.bytedance.msdk.api.TTAdConstant;
import com.bytedance.msdk.api.TTMediationAdSdk;
import com.bytedance.msdk.api.TTPrivacyConfig;
import com.bytedance.msdk.api.UserInfoForSegment;
import com.bytedance.sdk.openadsdk.TTCustomController;

import java.util.HashMap;
import java.util.Map;

public class TTAdManagerHolder {

    private static boolean sInit;

    public static TTPrivacyConfig privacyConfig = new TTPrivacyConfig() {
        @Override
        public boolean isLimitPersonalAds() {
            return true;
        }

        @Override
        public boolean isCanUsePhoneState() {
            return false;
        }

        @Override
        public boolean isCanUseLocation() {
            return false;
        }
    };

    public static void init(Context context,com.plugin.ad_mobrain.config.TTAdConfig config) {
        doInit(context, config);
    }

    public static void initUnitySdkBanner(Activity activity) {
        TTMediationAdSdk.initUnityForBanner(activity);
    }


    //step1:接入网盟广告sdk的初始化操作，详情见接入文档和穿山甲平台说明
    private static void doInit(Context context, com.plugin.ad_mobrain.config.TTAdConfig mapConfig) {
        if (!sInit) {
            TTMediationAdSdk.initialize(context, buildConfig(context, mapConfig));
            sInit = true;
        }
    }

    private static TTAdConfig buildConfig(Context context, com.plugin.ad_mobrain.config.TTAdConfig adConfig) {
        UserInfoForSegment userInfo = new UserInfoForSegment();
        userInfo.setUserId("msdk demo");
        userInfo.setGender(UserInfoForSegment.GENDER_MALE);
        userInfo.setChannel("msdk channel");
        userInfo.setSubChannel("msdk sub channel");
        userInfo.setAge(999);
        userInfo.setUserValueGroup("msdk demo user value group");

        Map<String, String> customInfos = new HashMap<>();
        customInfos.put("aaaa", "test111");
        customInfos.put("bbbb", "test222");
        userInfo.setCustomInfos(customInfos);
//        String APPID = mapConfig.get("appId");
//        String APPNAME = mapConfig.get("appName");
//        Boolean isDebug = mapConfig.get()
        return new TTAdConfig.Builder()
                .appId(adConfig.getADD_ID()) //必填 ，不能为空
                .appName(adConfig.getAPP_NAME()) //必填，不能为空
                .openAdnTest(false)//开启第三方ADN测试时需要设置为true，会每次重新拉去最新配置，release 包情况下必须关闭.默认false
                .isPanglePaid(false)//是否为费用户
                .setPublisherDid(getAndroidId(context)) //用户自定义device_id
                .openDebugLog(adConfig.getOPEN_DEBUG()) //测试阶段打开，可以通过日志排查问题，上线时去除该调用
                .usePangleTextureView(adConfig.getUSE_PANGLE_TEXT_URE_VIEW()) //使用TextureView控件播放视频,默认为SurfaceView,当有SurfaceView冲突的场景，可以使用TextureView
                .setPangleTitleBarTheme(TTAdConstant.TITLE_BAR_THEME_DARK)
                .allowPangleShowNotify(adConfig.getALLOW_PANGLE_SHOW_NOTIFY()) //是否允许sdk展示通知栏提示
                .allowPangleShowPageWhenScreenLock(true) //是否在锁屏场景支持展示广告落地页
                .setPangleDirectDownloadNetworkType(TTAdConstant.NETWORK_STATE_WIFI, TTAdConstant.NETWORK_STATE_3G) //允许直接下载的网络状态集合
                .needPangleClearTaskReset()//特殊机型过滤，部分机型出现包解析失败问题（大部分是三星）。参数取android.os.Build.MODEL
                .setUserInfoForSegment(userInfo) // 设置流量分组的信息
//                .customController(new TTCustomController() {}) // 该函数已经废弃，请不要使用。使用setPrivacyConfig进行权限设置
                .setPrivacyConfig(privacyConfig)
                .build();
    }

    public static String getAndroidId(Context context) {
        String androidId = null;
        try {
            androidId = Settings.System.getString(context.getContentResolver(), Settings.System.ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return androidId;
    }
}
