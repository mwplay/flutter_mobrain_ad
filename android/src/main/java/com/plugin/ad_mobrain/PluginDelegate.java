package com.plugin.ad_mobrain;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;

import com.github.gzuliyujiang.oaid.DeviceID;
import com.github.gzuliyujiang.oaid.IGetter;
import com.plugin.ad_mobrain.config.TTAdConfig;
import com.plugin.ad_mobrain.config.TTAdManagerHolder;
import com.plugin.ad_mobrain.page.CustomBannerAdFactory;
import com.plugin.ad_mobrain.page.CustomNativeAdFactory;
import com.plugin.ad_mobrain.page.FullScreenVideoAd;
import com.plugin.ad_mobrain.page.InterstitialPage;
import com.plugin.ad_mobrain.page.RewardVideoPage;
import com.plugin.ad_mobrain.page.SplashActivity;
import com.plugin.ad_mobrain.utils.MainThreadResult;

import org.jetbrains.annotations.NotNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

public class PluginDelegate implements MethodChannel.MethodCallHandler, EventChannel.StreamHandler {
    // Flutter 插件绑定对象
    public FlutterPlugin.FlutterPluginBinding bind;
    // 当前 Activity
    public Activity activity;
    // 返回通道
    private MethodChannel.Result result;
    // 事件通道
    private EventChannel.EventSink eventSink;
    // 插件代理对象
    private static PluginDelegate _instance;

    public static PluginDelegate getInstance() {
        return _instance;
    }

    private RewardVideoPage rewardPage;


    public PluginDelegate(Activity activity, FlutterPlugin.FlutterPluginBinding bind) {
        this.activity = activity;
        this.bind = bind;
        _instance = this;
    }

    /**
     * 建立事件通道监听
     *
     * @param arguments 参数
     * @param events    事件回调对象
     */
    @Override
    public void onListen(Object arguments, EventChannel.EventSink events) {
        eventSink = events;
    }

    @Override
    public void onCancel(Object arguments) {
        eventSink = null;
    }

    @Override
    public void onMethodCall(@NonNull @org.jetbrains.annotations.NotNull MethodCall call, @NonNull @org.jetbrains.annotations.NotNull MethodChannel.Result result) {
        String method = call.method;
        final MainThreadResult mainThreadResult = new MainThreadResult(result);
        if (method.equals("initAd")) {
            adInit(call, result);
        } else if (method.equals("InterstitialAd")) {
            showInterstitialAd(call, result);
        } else if (method.equals("oaid")) {
            getOaid(call, mainThreadResult);
        } else if (method.equals("RewardVideoAd")) {
            showRewardAd(call, mainThreadResult);
        } else if (method.equals("imei")) {
            getImei(call, result);
        } else if (method.equals("splashAd")) {
            SplashAd(call, result);
        } else if (method.equals("fullScreenVideoAd")) {
            FullScreenVideoAd(call, mainThreadResult);
        } else if (method.equals("getRewardStatus")) {
            getRewardStatus(call, mainThreadResult);
        } else {
            result.notImplemented();
        }
    }

    /**
     * 添加事件
     *
     * @param event 事件
     */
    public void addEvent(Object event) {
        if (eventSink != null) {
            eventSink.success(event);
        }
    }


    /**
     * 注册 Banner 广告
     */
    public void registerBannerView() {
        bind.getPlatformViewRegistry()
                .registerViewFactory("com.mobrain.banner", new CustomBannerAdFactory(this));
    }

    /**
     * 注册 native 广告
     */
    public void registerNativeView() {
        bind.getPlatformViewRegistry().registerViewFactory("com.mobrain.native", new CustomNativeAdFactory(this));
    }

    /**
     * 初始化广告
     */
    public void adInit(MethodCall call, MethodChannel.Result result) {
        String appId = call.argument("appId");
        String appName = call.argument("appName");
        Boolean openDebug = call.argument("openDebug");
        Boolean usePangleTextureView = call.argument("usePangleTextureView");
        Boolean allowPangleShowNotify = call.argument("allowPangleShowNotify");
        TTAdConfig adConfig = new TTAdConfig();
        adConfig.setADD_ID(appId);
        adConfig.setAPP_NAME(appName);
        adConfig.setOPEN_DEBUG(openDebug);
        adConfig.setALLOW_PANGLE_SHOW_NOTIFY(allowPangleShowNotify);
        adConfig.setUSE_PANGLE_TEXT_URE_VIEW(usePangleTextureView);

        Log.i("adConfig", adConfig.toString());
        TTAdManagerHolder.init(activity, adConfig);
        result.success(true);
    }

    /**
     * 显示激励广告
     */
    public void showRewardAd(MethodCall call, MainThreadResult result) {
        String curReward = call.argument("RewardVideoRewardType");
        int scenario = call.argument("scenario");
        boolean isPlay = call.argument("isPlay");
        if (isPlay) {
            if (rewardPage.getStatus()) {
                rewardPage.playAd(scenario, curReward);
//                rewardPage.showAd();
            }

            return;
        }
        String id = call.argument("RewardVideoId");
        String extra = call.argument("RewardVideoExtra");
        String userId = call.argument("userId");
        String rewardName = call.argument("rewardName");
        int rewardAmount = call.argument("rewardAmount");

        rewardPage = new RewardVideoPage(activity, id, userId, rewardName, rewardAmount, extra, curReward, scenario, result);
        rewardPage.showAd();
    }

    /*
     * 获取 激励广告状态
     */
    public void getRewardStatus(MethodCall call, MainThreadResult result) {
        if (rewardPage != null) {
            result.success(rewardPage.getStatus());
        } else {
            result.success(false);
        }
    }

    /**
     * 显示插屏、插屏全屏视频、插屏激励视频广告
     *
     * @param call   MethodCall
     * @param result Result
     */
    public void showInterstitialAd(MethodCall call, MethodChannel.Result result) {
        String id = call.argument("InterstitialId");
        InterstitialPage in = new InterstitialPage();
        try {
            in.loadInteractionAd(activity, id);
            result.success(true);
        } catch (Exception e) {
            Log.i("错误", e.getMessage());

        }

    }

    /**
     * 获取oaid
     */
    public void getOaid(MethodCall call, final MainThreadResult result) {
        if (DeviceID.supportedOAID(activity)) {
            DeviceID.getOAID(activity, new IGetter() {

                @Override
                public void onOAIDGetComplete(@NonNull @NotNull String value) {
                    result.success(value);
                }

                @Override
                public void onOAIDGetError(@NonNull @NotNull Exception error) {
                    result.success("");
                }
            });
        } else {
            result.success("");
        }
    }

    /**
     * 获取imei
     */
    public void getImei(MethodCall call, final MethodChannel.Result result) {
        String imei = DeviceID.getUniqueID(activity);
        result.success(imei);
    }

    /**
     * 开屏广告
     */
    public void SplashAd(MethodCall call, MethodChannel.Result result) {
        String splashId = call.argument("SplashId");
        String splashFilePath = call.argument("splashFilePath");
        String appId = call.argument("appId");
        String adNetworkSlotId = call.argument("adNetworkSlotId");
        Intent intent = new Intent();
        intent.setClass(activity, SplashActivity.class);
        intent.putExtra("splashId", splashId);
        intent.putExtra("splashFilePath", splashFilePath);
        intent.putExtra("appId", appId);
        intent.putExtra("adNetworkSlotId", adNetworkSlotId);
        activity.startActivity(intent);
    }

    /*
     * 全屏广告
     */
    public void FullScreenVideoAd(MethodCall call, MainThreadResult result) {
        String id = call.argument("fullScreenVideoVerticalId");
        String userId = call.argument("userId");
        String extra = call.argument("extra");
        int template = call.argument("template");
        String curReward = call.argument("curReward");
        int orientation = call.argument("orientation");
        FullScreenVideoAd ad = new FullScreenVideoAd(id, template, userId, extra, curReward, orientation, result);
        ad.loadAd();
    }


}
