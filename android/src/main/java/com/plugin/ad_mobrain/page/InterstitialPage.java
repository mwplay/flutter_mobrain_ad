package com.plugin.ad_mobrain.page;

import android.app.Activity;
import android.util.Log;

import com.bytedance.msdk.api.AdError;
import com.bytedance.msdk.api.AdSlot;
import com.bytedance.msdk.api.TTVideoOption;
import com.bytedance.msdk.api.interstitial.TTInterstitialAd;
import com.bytedance.msdk.api.interstitial.TTInterstitialAdListener;
import com.bytedance.msdk.api.interstitial.TTInterstitialAdLoadCallback;
import com.plugin.ad_mobrain.utils.AdResult;
import com.plugin.ad_mobrain.utils.VideoOptionUtil;

public class InterstitialPage {
    final private String TAG = "InterstitialAd";
    private TTInterstitialAd mInterstitialAd;
    private Activity activity;
    private String mAdUnitId;

    /**
     * 加载插屏广告
     */
    public void loadInteractionAd(Activity activit, String mAdUnitI) {
        activity = activit;
        mAdUnitId = mAdUnitI;
        //Context 必须传activity
        /*
          注：每次加载插屏广告的时候需要新建一个TTInterstitialAd，否则可能会出现广告填充问题
          （ 例如：mInterstitialAd = new TTInterstitialAd(this, adUnitId);）
         */
        mInterstitialAd = new TTInterstitialAd(activity, mAdUnitId);

        TTVideoOption videoOption = VideoOptionUtil.getTTVideoOption();

        //创建插屏广告请求参数AdSlot,具体参数含义参考文档
        AdSlot adSlot = new AdSlot.Builder()
                .setAdStyleType(AdSlot.TYPE_EXPRESS_AD) // 注意：插屏暂时支持模版类型，必须手动设置为AdSlot.TYPE_EXPRESS_AD
                .setTTVideoOption(videoOption)
                .setImageAdSize(600, 600) //根据广告平台选择的尺寸（目前该比例规格仅对穿山甲SDK生效，插屏广告支持的广告尺寸：  1:1, 3:2, 2:3）
                .build();
        //请求广告，调用插屏广告异步请求接口
        final Activity finalActivity = activity;
        mInterstitialAd.loadAd(adSlot, new TTInterstitialAdLoadCallback() {
            @Override
            public void onInterstitialLoadFail(AdError adError) {
                Log.i(TAG, adError.message);
                AdResult.Nativate(AdResult.onAdFailed, "插屏广告:" + adError.message + "详情请根据code:(" + adError.code + ") 在官方文档查询", TAG,null,null);
            }

            @Override
            public void onInterstitialLoad() {
                AdResult.Nativate(AdResult.onAdLoaded, "插屏广告: onadLoad", TAG,null,null);
                mInterstitialAd.setTTAdInterstitialListener(interstitialListener);
                mInterstitialAd.showAd(finalActivity);
            }
        });
    }

    TTInterstitialAdListener interstitialListener = new TTInterstitialAdListener() {

        /**
         * 广告展示
         */
        @Override
        public void onInterstitialShow() {
            AdResult.Nativate(AdResult.onAdShown, "插屏广告show", TAG,null,null);
        }

        /**
         * show失败回调。如果show时发现无可用广告（比如广告过期或者isReady=false），会触发该回调。
         * 开发者应该结合自己的广告加载、展示流程，在该回调里进行重新加载。
         * @param adError showFail的具体原因
         */
        @Override
        public void onInterstitialShowFail(AdError adError) {
//                Toast.makeText(mContext, "插屏广告展示失败", Toast.LENGTH_LONG).show();
            Log.d(TAG, "onInterstitialShowFail");

            // 开发者应该结合自己的广告加载、展示流程，在该回调里进行重新加载
            loadInteractionAd(activity, mAdUnitId);
        }


        /**
         * 广告被点击
         */
        @Override
        public void onInterstitialAdClick() {
            AdResult.Nativate(AdResult.onAdClick, "插屏广告click", TAG,null,null);
        }

        /**
         * 关闭广告
         */
        @Override
        public void onInterstitialClosed() {
            AdResult.Nativate(AdResult.onAdClosed, "插屏广告close", TAG,null,null);
        }


        /**
         * 当广告打开浮层时调用，如打开内置浏览器、内容展示浮层，一般发生在点击之后
         * 常常在onAdLeftApplication之前调用
         */
        @Override
        public void onAdOpened() {
            AdResult.Nativate(AdResult.onAdOpened, "插屏广告open", TAG,null,null);
        }


        /**
         * 此方法会在用户点击打开其他应用（例如 Google Play）时
         * 于 onAdOpened() 之后调用，从而在后台运行当前应用。
         */
        @Override
        public void onAdLeftApplication() {
            AdResult.Nativate(AdResult.onAdLeftApp, "插屏广告onadleft", TAG,null,null);
        }
    };


}
