package com.plugin.ad_mobrain.page;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.bytedance.msdk.api.AdError;
import com.bytedance.msdk.api.AdSlot;
import com.bytedance.msdk.api.TTAdConstant;
import com.bytedance.msdk.api.TTMediationAdSdk;
import com.bytedance.msdk.api.TTSettingConfigCallback;
import com.bytedance.msdk.api.TTVideoOption;
import com.bytedance.msdk.api.reward.RewardItem;
import com.bytedance.msdk.api.reward.TTRewardAd;
import com.bytedance.msdk.api.reward.TTRewardedAdListener;
import com.bytedance.msdk.api.reward.TTRewardedAdLoadCallback;
import com.plugin.ad_mobrain.utils.AdResult;
import com.plugin.ad_mobrain.utils.MainThreadResult;

import java.util.HashMap;
import java.util.Map;

public class RewardVideoPage {
    final private String TAG = "RewardedVideoAd";
    private Activity activity;
    private String adId;
    private TTRewardAd mttRewardAd;
    private boolean isVertical = true;
    private String mHorizontalCodeId;
    private String mVerticalCodeId;
    private String userId;
    private String rewardName; // 奖励名称
    private int rewardAmount; // 奖励数量
    private String extra; // 扩展参数
    private boolean loadSuccess;
    private String curReward;
    private int scenario;
    private MainThreadResult methodResult;


    public RewardVideoPage(Activity activit, String id, String userid, String reward_name, int rewardcount, String extras, String RewardVideoRewardType, int scree, MainThreadResult result) {
        adId = id;
        activity = activit;
        userId = userid;
        rewardName = reward_name;
        rewardAmount = rewardcount;
        extra = extras;
        curReward = RewardVideoRewardType;
        scenario = scree;
        methodResult = result;
        initAdUnitId();
    }

    private void initAdUnitId() {
        Intent intent = activity.getIntent();
        if (intent == null) {
            return;
        }
        mHorizontalCodeId = intent.getStringExtra("horizontal_adUnitId");
        mVerticalCodeId = intent.getStringExtra("vertical_adUnitId");
    }

    public void playAd(int scenari, String curRewar) {
        scenario = scenari;
        curReward = curRewar;
        if (getStatus()) {
            mttRewardAd.showRewardAd(activity, mTTRewardedAdListener);
        }
    }

    private TTSettingConfigCallback mSettingConfigCallback = new TTSettingConfigCallback() {
        @Override
        public void configLoad() {
            Log.e(TAG, "load ad 在config 回调中加载广告");
            loadAd(); //执行广告加载
        }
    };

    private void loadAdWithCallback() {
        /**
         * 判断当前是否存在config 配置 ，如果存在直接加载广告 ，如果不存在则注册config加载回调
         */
        if (TTMediationAdSdk.configLoadSuccess()) {
            Log.e(TAG, "load ad 当前config配置存在，直接加载广告");
            loadAd();
        } else {
            Log.e(TAG, "load ad 当前config配置不存在，正在请求config配置....");
            TTMediationAdSdk.registerConfigCallback(mSettingConfigCallback); //不能使用内部类，否则在ondestory中无法移除该回调
        }
    }

    public void showAd() {
        loadAdWithCallback();
    }

    public boolean getStatus() {
        if (mttRewardAd != null) {
            Log.i("广告类型:  广告状态---》", mttRewardAd.isReady() + "");
            return loadSuccess && mttRewardAd.isReady();
        } else {
            return false;
        }
    }

    private void loadAd() {
        /*
         * 注：每次加载激励视频广告的时候需要新建一个TTRewardAd，否则可能会出现广告填充问题
         * （ 例如：mttRewardAd = new TTRewardAd(this, adUnitId);）
         */
        if (mttRewardAd != null) {

        } else {
            mttRewardAd = new TTRewardAd(activity, adId);
        }


        TTVideoOption videoOption = new TTVideoOption.Builder()
                .setMuted(true)//对所有SDK的激励广告生效，除需要在平台配置的SDK，如穿山甲SDK
                .setAdmobAppVolume(0f)//配合Admob的声音大小设置[0-1]
                .build();

        Map<String, String> customData = new HashMap<>();
        customData.put(AdSlot.CUSTOM_DATA_KEY_PANGLE, extra);
        customData.put(AdSlot.CUSTOM_DATA_KEY_GDT, extra);
//        customData.put(AdSlot.CUSTOM_DATA_KEY_KS, "ks custom data");
        // 其他需要透传给adn的数据。

        //创建广告请求参数AdSlot,具体参数含义参考文档
        AdSlot.Builder adSlotBuilder = new AdSlot.Builder()
                .setTTVideoOption(videoOption)
                .setRewardName(rewardName) //奖励的名称
                .setRewardAmount(rewardAmount)  //奖励的数量
                .setUserID(userId)//用户id,必传参数
                .setAdStyleType(AdSlot.TYPE_EXPRESS_AD) // 确保该聚合广告位下所有gdt代码位都是同一种类型（模版或非模版）。
                .setCustomData(customData) // 激励视频开启服务端验证时，透传给adn的自定义数据。
                .setOrientation(TTAdConstant.VERTICAL);//必填参数，期望视频的播放方向：TTAdConstant.HORIZONTAL 或 TTAdConstant.VERTICAL

        //请求广告
        mttRewardAd.loadRewardAd(adSlotBuilder.build(), new TTRewardedAdLoadCallback() {

            @Override
            public void onRewardVideoLoadFail(AdError adError) {
                loadSuccess = false;
                methodResult.success(false);
                AdResult.Nativate(AdResult.onAdFailed, "激励广告:" + adError.message + "详情请根据code:(" + adError.code + ") 在官方文档查询", TAG, curReward, null);
                onDestory();
            }

            @Override
            public void onRewardVideoAdLoad() {
                loadSuccess = true;
                methodResult.success(true);
                AdResult.Nativate(AdResult.onAdLoaded, "激励广告: onadLoad", TAG, curReward, null);
                if (scenario == 999) {
                    mttRewardAd.showRewardAd(activity, mTTRewardedAdListener);
                }
            }

            @Override
            public void onRewardVideoCached() {
                Log.d(TAG, "onRewardVideoCached....缓存成功" + mttRewardAd.isReady());
                loadSuccess = true;
                AdResult.Nativate(AdResult.onRewardVideoCached, "激励广告: onRewardVideoCached scenario: " + scenario + "-->curReward: " + curReward, TAG, curReward, null);
//                TToast.show(RewardVideoActivity.this, "激励视频素材缓存成功！");
            }
        });
    }


    /**
     * 激励视频交互回调
     */
    private TTRewardedAdListener mTTRewardedAdListener = new TTRewardedAdListener() {

        /**
         * 广告的展示回调 每个广告仅回调一次
         */
        public void onRewardedAdShow() {
//            TToast.show(RewardVideoActivity.this, "激励onRewardedAdShow！");
            AdResult.Nativate(AdResult.onAdShown, "激励show", TAG, curReward, null);

        }

        /**
         * show失败回调。如果show时发现无可用广告（比如广告过期或者isReady=false），会触发该回调。
         * 开发者应该结合自己的广告加载、展示流程，在该回调里进行重新加载。
         * @param adError showFail的具体原因
         */
        @Override
        public void onRewardedAdShowFail(AdError adError) {
            AdResult.Nativate(AdResult.onAdFailed, "激励广告:" + adError.message + "详情请根据code:(" + adError.code + ") 在官方文档查询", TAG, curReward, null);

            // 开发者应该结合自己的广告加载、展示流程，在该回调里进行重新加载

        }

        /**
         * 注意Admob的激励视频不会回调该方法
         */
        @Override
        public void onRewardClick() {
            AdResult.Nativate(AdResult.onAdClick, "激励广告Click", TAG, curReward, null);

        }

        /**
         * 广告关闭的回调
         */
        public void onRewardedAdClosed() {
            loadSuccess = false;
            AdResult.Nativate(AdResult.onAdClosed, "激励广告close", TAG, curReward, null);
            onDestory();

        }

        /**
         * 视频播放完毕的回调 Admob广告不存在该回调
         */
        public void onVideoComplete() {
            Log.d(TAG, "onVideoComplete");

        }

        /**
         * 1、视频播放失败的回调
         */
        public void onVideoError() {
            Log.d(TAG, "onVideoError");

        }

        /**
         * 激励视频播放完毕，验证是否有效发放奖励的回调
         */
        public void onRewardVerify(RewardItem rewardItem) {
        }

        /**
         * - Mintegral GDT Admob广告不存在该回调
         */
        @Override
        public void onSkippedVideo() {

        }

    };

    private void onDestory() {
        loadSuccess = false;
        TTMediationAdSdk.unregisterConfigCallback(mSettingConfigCallback);
        if (mttRewardAd != null) {
            mttRewardAd.destroy();
            mttRewardAd = null;
        }
    }
}
