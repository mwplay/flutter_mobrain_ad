package com.plugin.ad_mobrain.page;

import android.view.View;
import android.widget.FrameLayout;

import com.bytedance.msdk.api.AdError;
import com.bytedance.msdk.api.AdSlot;
import com.bytedance.msdk.api.TTAdSize;
import com.bytedance.msdk.api.banner.TTAdBannerListener;
import com.bytedance.msdk.api.banner.TTAdBannerLoadCallBack;
import com.bytedance.msdk.api.banner.TTBannerViewAd;
import com.plugin.ad_mobrain.PluginDelegate;
import com.plugin.ad_mobrain.utils.AdResult;

import java.util.Map;
import java.util.Objects;

import io.flutter.plugin.platform.PlatformView;

public class CustomBannerAd implements PlatformView {
    private  FrameLayout layout;
    private PluginDelegate pluginDelegate;
    private TTBannerViewAd mTTBannerViewAd;
    final private static String TAG = "BannerAd";
    private String adId = "";
    private int width = 0;
    private int height = 0;
    CustomBannerAd(final PluginDelegate activity, Map<String, Object> creationParams) {
        this.pluginDelegate = activity;

        adId = Objects.requireNonNull(creationParams.get("bannerId")).toString();
        width = Integer.parseInt(Objects.requireNonNull(creationParams.get("width")).toString());
        height =Integer.parseInt(Objects.requireNonNull(creationParams.get("height")).toString());
        layout = new FrameLayout(pluginDelegate.activity);

        loadBannerAd();


    }


    private void loadBannerAd() {
        /*
         * 注：每次加载banner的时候需要新建一个TTBannerViewAd，否则可能会出现广告填充问题
         * （ 例如：mTTBannerViewAd = new TTBannerViewAd(this, adUnitId);）
         */
        mTTBannerViewAd = new TTBannerViewAd(pluginDelegate.activity, adId);
        mTTBannerViewAd.setRefreshTime(30);
        mTTBannerViewAd.setAllowShowCloseBtn(true);//如果广告本身允许展示关闭按钮，这里设置为true就是展示。注：目前只有mintegral支持。
        mTTBannerViewAd.setTTAdBannerListener(ttAdBannerListener);
        //step4:创建广告请求参数AdSlot,具体参数含义参考文档

        AdSlot adSlot = new AdSlot.Builder()
                .setAdStyleType(AdSlot.TYPE_EXPRESS_AD) // banner暂时只支持模版类型，必须手动设置为AdSlot.TYPE_EXPRESS_AD
//                .setBannerSize(TTAdSize.BANNER_300_250)
                .setBannerSize(TTAdSize.BANNER_CUSTOME) // 使用TTAdSize.BANNER_CUSTOME可以调用setImageAdSize设置大小
                .setImageAdSize(width, height)
                .build();
        //step5:请求广告，对请求回调的广告作渲染处理
        mTTBannerViewAd.loadAd(adSlot, new TTAdBannerLoadCallBack() {
            @Override
            public void onAdFailedToLoad(AdError adError) {
                AdResult.Nativate(AdResult.onAdFailed, "banner广告:" + adError.message + "详情请根据code:(" + adError.code + ") 在官方文档查询", TAG,null,null);
                layout.removeAllViews();
            }

            @Override
            public void onAdLoaded() {
                layout.removeAllViews();
                AdResult.Nativate(AdResult.onAdLoaded, "banner广告onload", TAG ,null,null);
                if (mTTBannerViewAd != null) {
                    //横幅广告容器的尺寸必须至少与横幅广告一样大。如果您的容器留有内边距，实际上将会减小容器大小。如果容器无法容纳横幅广告，则横幅广告不会展示
                    View view = mTTBannerViewAd.getBannerView();
                    if (view != null)
                        layout.addView(view);

                }
            }
        });

    }

    @Override
    public View getView() {
        return layout;
    }

    @Override
    public void dispose() {
        if (mTTBannerViewAd != null) {
            mTTBannerViewAd.destroy();
        }
        layout.removeAllViews();
    }

    private TTAdBannerListener ttAdBannerListener = new TTAdBannerListener() {

        @Override
        public void onAdOpened() {
            AdResult.Nativate(AdResult.onAdOpened, "banner广告open", TAG,null,null);

        }

        @Override
        public void onAdLeftApplication() {
            AdResult.Nativate(AdResult.onAdLeftApp, "banner广告onadleft", TAG,null,null);

        }

        @Override
        public void onAdClosed() {
            AdResult.Nativate(AdResult.onAdClosed, "banner广告close", TAG,null,null);

        }

        @Override
        public void onAdClicked() {
            AdResult.Nativate(AdResult.onAdClick, "banner广告click", TAG,null,null);

        }

        @Override
        public void onAdShow() {
            AdResult.Nativate(AdResult.onAdShown, "banner广告show", TAG ,null,null);
        }

        /**
         * show失败回调。如果show时发现无可用广告（比如广告过期），会触发该回调。
         * 开发者应该结合自己的广告加载、展示流程，在该回调里进行重新加载。
         * @param adError showFail的具体原因
         */
        @Override
        public void onAdShowFail(AdError adError) {
            loadBannerAd();
        }
    };
}
