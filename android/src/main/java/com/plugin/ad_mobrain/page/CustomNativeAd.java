package com.plugin.ad_mobrain.page;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bytedance.msdk.api.AdError;
import com.bytedance.msdk.api.AdSlot;
import com.bytedance.msdk.api.AdmobNativeAdOptions;
import com.bytedance.msdk.api.TTAdConstant;
import com.bytedance.msdk.api.TTAdDislike;
import com.bytedance.msdk.api.TTAdSize;
import com.bytedance.msdk.api.TTDislikeCallback;
import com.bytedance.msdk.api.TTVideoOption;
import com.bytedance.msdk.api.nativeAd.TTNativeAd;
import com.bytedance.msdk.api.nativeAd.TTNativeAdAppInfo;
import com.bytedance.msdk.api.nativeAd.TTNativeAdListener;
import com.bytedance.msdk.api.nativeAd.TTNativeAdLoadCallback;
import com.bytedance.msdk.api.nativeAd.TTNativeExpressAdListener;
import com.bytedance.msdk.api.nativeAd.TTUnifiedNativeAd;
import com.bytedance.msdk.api.nativeAd.TTVideoListener;
import com.bytedance.msdk.api.nativeAd.TTViewBinder;
import com.plugin.ad_mobrain.PluginDelegate;
import com.plugin.ad_mobrain.R;
import com.plugin.ad_mobrain.utils.AdResult;
import com.plugin.ad_mobrain.utils.UIUtils;
import com.plugin.ad_mobrain.utils.VideoOptionUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.flutter.plugin.platform.PlatformView;

public class CustomNativeAd implements PlatformView {
    private FrameLayout layout;
    private PluginDelegate pluginDelegate;
    private static String TAG = "NativeAd";
    private TTUnifiedNativeAd mTTAdNative;
    private String mAdUnitId; // 广告id
    private int mAdStyle; // 1.模板广告  2. 原生广告
    private int width;
    private int height;
    private int adCount; // 请求广告数量
    private Context mContext;
    private ViewGroup viewGroup;
    private String nativeKey;

    CustomNativeAd(final PluginDelegate activity, Map<String, Object> creationParams) {
        layout = new FrameLayout(activity.activity);
        this.pluginDelegate = activity;
        mContext = activity.activity;
        mAdUnitId = creationParams.get("nativeId").toString();
        mAdStyle = Integer.parseInt(creationParams.get("mAdStyle").toString());
        width = Integer.parseInt(creationParams.get("width").toString());
        height = Integer.parseInt(creationParams.get("height").toString());
        adCount = Integer.parseInt(creationParams.get("adCount").toString());
//        viewGroup = (ViewGroup) LayoutInflater.from(activity.activity).inflate(R.layout.activity_native, null);
        nativeKey = creationParams.get("nativeKey").toString();
        loadNativeAd();
    }

    private void loadNativeAd() {
        mTTAdNative = new TTUnifiedNativeAd(pluginDelegate.activity, mAdUnitId);
        //视频声音控制设置
        TTVideoOption videoOption = VideoOptionUtil.getTTVideoOption();

        // 注意不设置默认使用 GDT Feed 模板渲染1.0,默认情况下无需执行下方代码
        if (mAdStyle == AdSlot.TYPE_EXPRESS_AD) {
            videoOption = VideoOptionUtil.getTTVideoOption2();
        }
        //针对Admob Native的特殊配置项
        AdmobNativeAdOptions admobNativeAdOptions = new AdmobNativeAdOptions();
        admobNativeAdOptions.setAdChoicesPlacement(AdmobNativeAdOptions.ADCHOICES_TOP_RIGHT)//设置广告小标默认情况下，广告选择叠加层会显示在右上角。
                .setRequestMultipleImages(true)//素材中可能包含多张图片，如果此值设置为true， 则表示需要展示多张图片，此值设置为 false（默认），仅提供第一张图片。
                .setReturnUrlsForImageAssets(true);//设置为true，SDK会仅提供Uri字段的值，允许自行决定是否下载实际图片，同时不会提供Drawable字段的值

        // 针对Gdt Native自渲染广告，可以自定义gdt logo的布局参数。该参数可选,非必须。
        FrameLayout.LayoutParams gdtNativeAdLogoParams =
                new FrameLayout.LayoutParams(
                        UIUtils.dip2px(pluginDelegate.activity.getApplicationContext(), 40),
                        UIUtils.dip2px(pluginDelegate.activity.getApplicationContext(), 13),
                        Gravity.RIGHT | Gravity.TOP); // 例如，放在右上角

        /**
         * 创建feed广告请求类型参数AdSlot,具体参数含义参考文档
         * 备注
         * 1: 如果是信息流自渲染广告，设置广告图片期望的图片宽高 ，不能为0
         * 2:如果是信息流模板广告，宽度设置为希望的宽度，高度设置为0(0为高度选择自适应参数)
         */

        AdSlot adSlot = new AdSlot.Builder()
                .setTTVideoOption(videoOption)//视频声音相关的配置
                .setAdmobNativeAdOptions(admobNativeAdOptions)
                .setAdStyleType(mAdStyle)//必传，表示请求的模板广告还是原生广告，AdSlot.TYPE_EXPRESS_AD：模板广告 ； AdSlot.TYPE_NATIVE_AD：原生广告
                // 备注
                // 1:如果是信息流自渲染广告，设置广告图片期望的图片宽高 ，不能为0
                // 2:如果是信息流模板广告，宽度设置为希望的宽度，高度设置为0(0为高度选择自适应参数)
                .setImageAdSize(width, height)// 必选参数 单位dp ，详情见上面备注解释
                .setAdCount(adCount) //请求广告数量为1到3条
                .setGdtNativeAdLogoParams(gdtNativeAdLogoParams) // 设置gdt logo布局参数。
                .build();

        mTTAdNative.loadAd(adSlot, new TTNativeAdLoadCallback() {
            @Override
            public void onAdLoaded(List<TTNativeAd> list) {
                Log.i("AD", list.toString());
                if (list != null) {
                    Log.i("AD", "添加view");
                    final FrameLayout.LayoutParams viewParam = new FrameLayout.LayoutParams(
                            Resources.getSystem().getDisplayMetrics().widthPixels,
                            ViewGroup.LayoutParams.MATCH_PARENT);
                    View v = getViewl(list.get(0), viewGroup, null);
                    Log.i("AD", v.toString());
                    layout.addView(v, viewParam);
                }
            }


            @Override
            public void onAdLoadedFial(AdError adError) {
                AdResult.Nativate(AdResult.onAdFailed, "native广告:" + adError.message + "详情请根据code:(" + adError.code + ") 在官方文档查询", TAG,null,nativeKey);
            }
        });
    }

    public View getViewl(TTNativeAd ad, View convertView, ViewGroup parent) {
        final int ITEM_VIEW_TYPE_NORMAL = 0;
        final int ITEM_VIEW_TYPE_GROUP_PIC_AD = 1;
        final int ITEM_VIEW_TYPE_SMALL_PIC_AD = 2;
        final int ITEM_VIEW_TYPE_LARGE_PIC_AD = 3;
        final int ITEM_VIEW_TYPE_VIDEO = 4;
        final int ITEM_VIEW_TYPE_VIDEO_VERTICAL = 7; //竖版视频
        final int ITEM_VIEW_TYPE_VERTICAL_IMG = 5;//竖版图片
        final int ITEM_VIEW_TYPE_EXPRESS_AD = 6;//竖版图片
        View v = new View(mContext);
        Log.i("getItemViewType", getItemViewType(ad) + "");
        switch (getItemViewType(ad)) {
            case ITEM_VIEW_TYPE_SMALL_PIC_AD:
                return getSmallAdView(convertView, parent, ad);
            case ITEM_VIEW_TYPE_LARGE_PIC_AD:
                return getLargeAdView(convertView, parent, ad);
            case ITEM_VIEW_TYPE_GROUP_PIC_AD:
                return getGroupAdView(convertView, parent, ad);
            case ITEM_VIEW_TYPE_VIDEO:
            case ITEM_VIEW_TYPE_VIDEO_VERTICAL:
                return getVideoView(convertView, parent, ad);
            case ITEM_VIEW_TYPE_VERTICAL_IMG:
                return getVerticalAdView(convertView, parent, ad);
            case ITEM_VIEW_TYPE_EXPRESS_AD:
                return getExpressAdView(convertView, parent, ad);
            default:
                return v;
        }
    }

    public int getItemViewType(TTNativeAd ad) {
        final int ITEM_VIEW_TYPE_NORMAL = 0;
        final int ITEM_VIEW_TYPE_GROUP_PIC_AD = 1;
        final int ITEM_VIEW_TYPE_SMALL_PIC_AD = 2;
        final int ITEM_VIEW_TYPE_LARGE_PIC_AD = 3;
        final int ITEM_VIEW_TYPE_VIDEO = 4;
        final int ITEM_VIEW_TYPE_VIDEO_VERTICAL = 7; //竖版视频
        final int ITEM_VIEW_TYPE_VERTICAL_IMG = 5;//竖版图片
        final int ITEM_VIEW_TYPE_EXPRESS_AD = 6;//竖版图片
        //模板广告特殊处理
        if (ad != null && ad.isExpressAd()) {
            return ITEM_VIEW_TYPE_EXPRESS_AD;
        }
        if (ad == null) {
            return ITEM_VIEW_TYPE_NORMAL;
        } else if (ad.getAdImageMode() == TTAdConstant.IMAGE_MODE_SMALL_IMG) {
            return ITEM_VIEW_TYPE_SMALL_PIC_AD;
        } else if (ad.getAdImageMode() == TTAdConstant.IMAGE_MODE_LARGE_IMG) {
            return ITEM_VIEW_TYPE_LARGE_PIC_AD;
        } else if (ad.getAdImageMode() == TTAdConstant.IMAGE_MODE_GROUP_IMG) {
            return ITEM_VIEW_TYPE_GROUP_PIC_AD;
        } else if (ad.getAdImageMode() == TTAdConstant.IMAGE_MODE_VIDEO) {
            return ITEM_VIEW_TYPE_VIDEO;
        } else if (ad.getAdImageMode() == TTAdConstant.IMAGE_MODE_VERTICAL_IMG) {
            return ITEM_VIEW_TYPE_VERTICAL_IMG;
        } else if (ad.getAdImageMode() == TTAdConstant.IMAGE_MODE_VIDEO_VERTICAL) {
            return ITEM_VIEW_TYPE_VIDEO_VERTICAL;
        } else {
            return ITEM_VIEW_TYPE_NORMAL;
        }
    }

    @Override
    public View getView() {
        return layout;
    }

    @Override
    public void dispose() {
        if (mTTAdNative != null) {
            mTTAdNative.destroy();
        }
        layout.removeAllViews();
    }


    private void bindData(View convertView, final AdViewHolder adViewHolder, final TTNativeAd ad, TTViewBinder viewBinder) {
        //设置dislike弹窗，如果有
        if (ad.hasDislike()) {
            final TTAdDislike ttAdDislike = ad.getDislikeDialog((Activity) mContext);
            adViewHolder.mDislike.setVisibility(View.VISIBLE);
            adViewHolder.mDislike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //使用接口来展示
                    ttAdDislike.showDislikeDialog();
                    ttAdDislike.setDislikeCallback(new TTDislikeCallback() {
                        @Override
                        public void onSelected(int position, String value) {
                            AdResult.Nativate(AdResult.onAdClosed, "native广告close", TAG,null,nativeKey);
                        }

                        @Override
                        public void onCancel() {
//                            TToast.show(mContext, "dislike 点击了取消");
                        }

                        /**
                         * 拒绝再次提交
                         */
                        @Override
                        public void onRefuse() {

                        }

                        @Override
                        public void onShow() {

                        }
                    });
                }
            });
        } else {
            if (adViewHolder.mDislike != null)
                adViewHolder.mDislike.setVisibility(View.GONE);
        }

        setDownLoadAppInfo(ad, adViewHolder);

        //设置事件回调
        ad.setTTNativeAdListener(mTTNativeAdListener);
        //可以被点击的view, 也可以把convertView放进来意味item可被点击
        List<View> clickViewList = new ArrayList<>();
        clickViewList.add(convertView);
        clickViewList.add(adViewHolder.mSource);
        clickViewList.add(adViewHolder.mTitle);
        clickViewList.add(adViewHolder.mDescription);
        clickViewList.add(adViewHolder.mIcon);
        //添加点击区域
        if (adViewHolder instanceof LargeAdViewHolder) {
            clickViewList.add(((LargeAdViewHolder) adViewHolder).mLargeImage);
        } else if (adViewHolder instanceof SmallAdViewHolder) {
            clickViewList.add(((SmallAdViewHolder) adViewHolder).mSmallImage);
        } else if (adViewHolder instanceof VerticalAdViewHolder) {
            clickViewList.add(((VerticalAdViewHolder) adViewHolder).mVerticalImage);
        } else if (adViewHolder instanceof VideoAdViewHolder) {
            clickViewList.add(((VideoAdViewHolder) adViewHolder).videoView);
        } else if (adViewHolder instanceof GroupAdViewHolder) {
            clickViewList.add(((GroupAdViewHolder) adViewHolder).mGroupImage1);
            clickViewList.add(((GroupAdViewHolder) adViewHolder).mGroupImage2);
            clickViewList.add(((GroupAdViewHolder) adViewHolder).mGroupImage3);
        }
        //触发创意广告的view（点击下载或拨打电话）
        List<View> creativeViewList = new ArrayList<>();
        creativeViewList.add(adViewHolder.mCreativeButton);
        //重要! 这个涉及到广告计费，必须正确调用。convertView必须使用ViewGroup。
        ad.registerView((ViewGroup) convertView, clickViewList, creativeViewList, viewBinder);

        adViewHolder.mTitle.setText(ad.getTitle()); //title为广告的简单信息提示
        adViewHolder.mDescription.setText(ad.getDescription()); //description为广告的较长的说明
        adViewHolder.mSource.setText(TextUtils.isEmpty(ad.getSource()) ? "广告来源" : ad.getSource());

        String icon = ad.getIconUrl();
        if (icon != null) {
            Glide.with(mContext).load(icon).into(adViewHolder.mIcon);
        }
        Button adCreativeButton = adViewHolder.mCreativeButton;
        switch (ad.getInteractionType()) {
            case TTAdConstant.INTERACTION_TYPE_DOWNLOAD:
                adCreativeButton.setVisibility(View.VISIBLE);
//                adCreativeButton.setText(TextUtils.isEmpty(ad.getActionText()) ? "立即下载" : ad.getActionText());
                break;
            case TTAdConstant.INTERACTION_TYPE_DIAL:
                adCreativeButton.setVisibility(View.VISIBLE);
                adCreativeButton.setText("立即拨打");
                break;
            case TTAdConstant.INTERACTION_TYPE_LANDING_PAGE:
            case TTAdConstant.INTERACTION_TYPE_BROWSER:
                adCreativeButton.setVisibility(View.VISIBLE);
//                adCreativeButton.setText(TextUtils.isEmpty(ad.getActionText()) ? "查看详情" : ad.getActionText());
                break;
            default:
                adCreativeButton.setVisibility(View.GONE);
//                TToast.show(mContext, "交互类型异常");
        }
    }


    private void setDownLoadAppInfo(TTNativeAd ttNativeAd, AdViewHolder adViewHolder) {
        if (adViewHolder == null) {
            return;
        }
        if (ttNativeAd == null || ttNativeAd.getNativeAdAppInfo() == null) {
            adViewHolder.app_info.setVisibility(View.GONE);
        } else {
            adViewHolder.app_info.setVisibility(View.VISIBLE);
            TTNativeAdAppInfo appInfo = ttNativeAd.getNativeAdAppInfo();
            adViewHolder.app_name.setText(appInfo.getAppName());
            adViewHolder.author_name.setText(appInfo.getAuthorName());
            adViewHolder.package_size.setText("包大小：" + appInfo.getPackageSizeBytes());
            adViewHolder.permissions_url.setText(appInfo.getPermissionsUrl());
            adViewHolder.privacy_agreement.setText(appInfo.getPrivacyAgreement());
            adViewHolder.version_name.setText("版本号：" + appInfo.getVersionName());
        }
    }

    TTNativeAdListener mTTNativeAdListener = new TTNativeAdListener() {
        @Override
        public void onAdClick() {
            AdResult.Nativate(AdResult.onAdClick, "native广告click", TAG,null,nativeKey);
        }


        @Override
        public void onAdShow() {
            Log.i("AD", "native广告show");
            AdResult.Nativate(AdResult.onAdShown, "native广告show", TAG,null,nativeKey);
        }
    };

    //渲染模板广告
    @SuppressWarnings("RedundantCast")
    private View getExpressAdView(View convertView, ViewGroup parent, @NonNull final TTNativeAd ad) {
        final ExpressAdViewHolder adViewHolder;
        try {
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.listitem_ad_native_express, parent, false);
                adViewHolder = new ExpressAdViewHolder();
                adViewHolder.mAdContainerView = (FrameLayout) convertView.findViewById(R.id.iv_listitem_express);
                convertView.setTag(adViewHolder);
            } else {
                adViewHolder = (ExpressAdViewHolder) convertView.getTag();
            }

            //判断是否存在dislike按钮
            if (ad.hasDislike()) {
                ad.setDislikeCallback((Activity) mContext, new TTDislikeCallback() {
                    @Override
                    public void onSelected(int position, String value) {
                        AdResult.Nativate(AdResult.onAdClosed, "native广告close", TAG,null,nativeKey);
//                        TToast.show(mContext, "点击 " + value);
//                        //用户选择不喜欢原因后，移除广告展示
//                        mData.remove(ad);
//                        sumCount--;
//                        notifyDataSetChanged();
                    }

                    @Override
                    public void onCancel() {
//                        TToast.show(mContext, "dislike 点击了取消");
//                        Log.d(TAG, "dislike 点击了取消");
                    }

                    /**
                     * 拒绝再次提交
                     */
                    @Override
                    public void onRefuse() {

                    }

                    @Override
                    public void onShow() {
                        AdResult.Nativate(AdResult.onAdShown, "native广告onadshow", TAG,null,nativeKey);
                    }
                });
            }

            //设置点击展示回调监听
            ad.setTTNativeAdListener(new TTNativeExpressAdListener() {
                @Override
                public void onAdClick() {
                    Log.d(TAG, "onAdClick");
//                    TToast.show(mContext, "模板广告被点击");
                }

                @Override
                public void onAdShow() {
                    AdResult.Nativate(AdResult.onAdShown, "native广告onadshow", TAG,null,nativeKey);
                    Log.d(TAG, "onAdShow");
//                    TToast.show(mContext, "模板广告show");

                }

                @Override
                public void onRenderFail(View view, String msg, int code) {
//                    TToast.show(mContext, "模板广告渲染失败code=" + code + ",msg=" + msg);
                    Log.d(TAG, "onRenderFail   code=" + code + ",msg=" + msg);

                }

                // ** 注意点 ** 不要在广告加载成功回调里进行广告view展示，要在onRenderSucces进行广告view展示，否则会导致广告无法展示。
                @Override
                public void onRenderSuccess(float width, float height) {
                    Log.d(TAG, "onRenderSuccess");
//                    TToast.show(mContext, "模板广告渲染成功:width=" + width + ",height=" + height);
                    //回调渲染成功后将模板布局添加的父View中
                    if (adViewHolder.mAdContainerView != null) {
                        //获取视频播放view,该view SDK内部渲染，在媒体平台可配置视频是否自动播放等设置。
                        int sWidth;
                        int sHeight;
                        /**
                         * 如果存在父布局，需要先从父布局中移除
                         */
                        final View video = ad.getExpressView(); // 获取广告view  如果存在父布局，需要先从父布局中移除
                        if (width == TTAdSize.FULL_WIDTH && height == TTAdSize.AUTO_HEIGHT) {
                            sWidth = FrameLayout.LayoutParams.MATCH_PARENT;
                            sHeight = FrameLayout.LayoutParams.WRAP_CONTENT;
                        } else {
                            sWidth = UIUtils.getScreenWidth(mContext);
                            sHeight = (int) ((sWidth * height) / width);
                        }
                        if (video != null) {
                            /**
                             * 如果存在父布局，需要先从父布局中移除
                             */
                            UIUtils.removeFromParent(video);
                            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(sWidth, sHeight);
                            adViewHolder.mAdContainerView.removeAllViews();
                            adViewHolder.mAdContainerView.addView(video, layoutParams);
                        }
                    }
                }
            });


            //视频广告设置播放状态回调（可选）
            ad.setTTVideoListener(new TTVideoListener() {

                @Override
                public void onVideoStart() {
//                    TToast.show(mContext, "模板广告视频开始播放");
                    Log.d(TAG, "onVideoStart");
                }

                @Override
                public void onVideoPause() {
//                    TToast.show(mContext, "模板广告视频暂停");
                    Log.d(TAG, "onVideoPause");

                }

                @Override
                public void onVideoResume() {
//                    TToast.show(mContext, "模板广告视频继续播放");
                    Log.d(TAG, "onVideoResume");

                }

                @Override
                public void onVideoCompleted() {
//                    TToast.show(mContext, "模板播放完成");
                    Log.d(TAG, "onVideoCompleted");
                }

                @Override
                public void onVideoError(AdError adError) {
//                    TToast.show(mContext, "模板广告视频播放出错");
//                    Log.d(TAG, "onVideoError");
                }
            });

            ad.render();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    /**
     * @param convertView
     * @param parent
     * @param ad
     * @return
     */
    private View getVerticalAdView(View convertView, ViewGroup parent, @NonNull final TTNativeAd ad) {
        VerticalAdViewHolder adViewHolder;
        TTViewBinder viewBinder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.listitem_ad_vertical_pic, parent, false);
            adViewHolder = new VerticalAdViewHolder();
            adViewHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_listitem_ad_title);
            adViewHolder.mSource = (TextView) convertView.findViewById(R.id.tv_listitem_ad_source);
            adViewHolder.mDescription = (TextView) convertView.findViewById(R.id.tv_listitem_ad_desc);
            adViewHolder.mVerticalImage = convertView.findViewById(R.id.iv_listitem_image);
            adViewHolder.mIcon = (ImageView) convertView.findViewById(R.id.iv_listitem_icon);
            adViewHolder.mDislike = (ImageView) convertView.findViewById(R.id.iv_listitem_dislike);
            adViewHolder.mCreativeButton = (Button) convertView.findViewById(R.id.btn_listitem_creative);
            adViewHolder.mLogo = convertView.findViewById(R.id.tt_ad_logo);//logoView 建议传入GroupView类型

            adViewHolder.app_info = convertView.findViewById(R.id.app_info);
            adViewHolder.app_name = convertView.findViewById(R.id.app_name);
            adViewHolder.author_name = convertView.findViewById(R.id.author_name);
            adViewHolder.package_size = convertView.findViewById(R.id.package_size);
            adViewHolder.permissions_url = convertView.findViewById(R.id.permissions_url);
            adViewHolder.privacy_agreement = convertView.findViewById(R.id.privacy_agreement);
            adViewHolder.version_name = convertView.findViewById(R.id.version_name);

            viewBinder = new TTViewBinder.Builder(R.layout.listitem_ad_vertical_pic)
                    .titleId(R.id.tv_listitem_ad_title)
                    .decriptionTextId(R.id.tv_listitem_ad_desc)
                    .mainImageId(R.id.iv_listitem_image)
                    .iconImageId(R.id.iv_listitem_icon)
                    .callToActionId(R.id.btn_listitem_creative)
                    .sourceId(R.id.tv_listitem_ad_source)
                    .logoLayoutId(R.id.tt_ad_logo)//logoView 建议传入GroupView类型
                    .build();
            adViewHolder.viewBinder = viewBinder;
            convertView.setTag(adViewHolder);
        } else {
            adViewHolder = (VerticalAdViewHolder) convertView.getTag();
            viewBinder = adViewHolder.viewBinder;
        }
        bindData(convertView, adViewHolder, ad, viewBinder);
        if (ad.getImageUrl() != null) {
            Glide.with(mContext).load(ad.getImageUrl()).into(adViewHolder.mVerticalImage);
        }

        return convertView;
    }

    //渲染视频广告，以视频广告为例，以下说明
    @SuppressWarnings("RedundantCast")
    private View getVideoView(View convertView, ViewGroup parent, @NonNull final TTNativeAd ad) {
        final VideoAdViewHolder adViewHolder;
        TTViewBinder viewBinder;
        try {
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.listitem_ad_large_video, parent, false);
                adViewHolder = new VideoAdViewHolder();
                adViewHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_listitem_ad_title);
                adViewHolder.mDescription = (TextView) convertView.findViewById(R.id.tv_listitem_ad_desc);
                adViewHolder.mSource = (TextView) convertView.findViewById(R.id.tv_listitem_ad_source);
                adViewHolder.videoView = (FrameLayout) convertView.findViewById(R.id.iv_listitem_video);
                adViewHolder.mIcon = (ImageView) convertView.findViewById(R.id.iv_listitem_icon);
                adViewHolder.mDislike = (ImageView) convertView.findViewById(R.id.iv_listitem_dislike);
                adViewHolder.mCreativeButton = (Button) convertView.findViewById(R.id.btn_listitem_creative);
                adViewHolder.mLogo = convertView.findViewById(R.id.tt_ad_logo);//logoView 建议传入GroupView类型

                adViewHolder.app_info = convertView.findViewById(R.id.app_info);
                adViewHolder.app_name = convertView.findViewById(R.id.app_name);
                adViewHolder.author_name = convertView.findViewById(R.id.author_name);
                adViewHolder.package_size = convertView.findViewById(R.id.package_size);
                adViewHolder.permissions_url = convertView.findViewById(R.id.permissions_url);
                adViewHolder.privacy_agreement = convertView.findViewById(R.id.privacy_agreement);
                adViewHolder.version_name = convertView.findViewById(R.id.version_name);

                //TTViewBinder 是必须类,需要开发者在确定好View之后把Id设置给TTViewBinder类，并在注册事件时传递给SDK
                viewBinder = new TTViewBinder.Builder(R.layout.listitem_ad_large_video).
                        titleId(R.id.tv_listitem_ad_title).
                        sourceId(R.id.tv_listitem_ad_source).
                        decriptionTextId(R.id.tv_listitem_ad_desc).
                        mediaViewIdId(R.id.iv_listitem_video).
                        callToActionId(R.id.btn_listitem_creative).
                        logoLayoutId(R.id.tt_ad_logo).//logoView 建议传入GroupView类型
                        iconImageId(R.id.iv_listitem_icon).build();
                adViewHolder.viewBinder = viewBinder;
                convertView.setTag(adViewHolder);
            } else {
                adViewHolder = (VideoAdViewHolder) convertView.getTag();
                viewBinder = adViewHolder.viewBinder;
            }

            //视频广告设置播放状态回调（可选）
            ad.setTTVideoListener(new TTVideoListener() {

                @Override
                public void onVideoStart() {
//                    TToast.show(mContext, "广告视频开始播放");
                    Log.d(TAG, "onVideoStart");
                }

                @Override
                public void onVideoPause() {
//                    TToast.show(mContext, "广告视频暂停");
                    Log.d(TAG, "onVideoPause");
                }

                @Override
                public void onVideoResume() {
//                    TToast.show(mContext, "广告视频继续播放");
                    Log.d(TAG, "onVideoResume");
                }

                @Override
                public void onVideoCompleted() {
//                    TToast.show(mContext, "广告播放完成");
                    Log.d(TAG, "onVideoCompleted");
                }

                @Override
                public void onVideoError(AdError adError) {
//                    TToast.show(mContext, "广告视频播放出错");
                    Log.d(TAG, "onVideoError");
                }

            });

            //绑定广告数据、设置交互回调
            bindData(convertView, adViewHolder, ad, viewBinder);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @SuppressWarnings("RedundantCast")
    private View getGroupAdView(View convertView, ViewGroup parent, @NonNull final TTNativeAd ad) {
        GroupAdViewHolder adViewHolder;
        TTViewBinder viewBinder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.listitem_ad_group_pic, parent, false);
            adViewHolder = new GroupAdViewHolder();
            adViewHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_listitem_ad_title);
            adViewHolder.mSource = (TextView) convertView.findViewById(R.id.tv_listitem_ad_source);
            adViewHolder.mDescription = (TextView) convertView.findViewById(R.id.tv_listitem_ad_desc);
            adViewHolder.mGroupImage1 = (ImageView) convertView.findViewById(R.id.iv_listitem_image1);
            adViewHolder.mGroupImage2 = (ImageView) convertView.findViewById(R.id.iv_listitem_image2);
            adViewHolder.mGroupImage3 = (ImageView) convertView.findViewById(R.id.iv_listitem_image3);
            adViewHolder.mIcon = (ImageView) convertView.findViewById(R.id.iv_listitem_icon);
            adViewHolder.mDislike = (ImageView) convertView.findViewById(R.id.iv_listitem_dislike);
            adViewHolder.mCreativeButton = (Button) convertView.findViewById(R.id.btn_listitem_creative);
            adViewHolder.mLogo = convertView.findViewById(R.id.tt_ad_logo);//logoView 建议传入GroupView类型

            adViewHolder.app_info = convertView.findViewById(R.id.app_info);
            adViewHolder.app_name = convertView.findViewById(R.id.app_name);
            adViewHolder.author_name = convertView.findViewById(R.id.author_name);
            adViewHolder.package_size = convertView.findViewById(R.id.package_size);
            adViewHolder.permissions_url = convertView.findViewById(R.id.permissions_url);
            adViewHolder.privacy_agreement = convertView.findViewById(R.id.privacy_agreement);
            adViewHolder.version_name = convertView.findViewById(R.id.version_name);

            viewBinder = new TTViewBinder.Builder(R.layout.listitem_ad_group_pic).
                    titleId(R.id.tv_listitem_ad_title).
                    decriptionTextId(R.id.tv_listitem_ad_desc).
                    sourceId(R.id.tv_listitem_ad_source).
                    mainImageId(R.id.iv_listitem_image1).//传第一张即可
                    logoLayoutId(R.id.tt_ad_logo).//logoView 建议传入GroupView类型
                    callToActionId(R.id.btn_listitem_creative).
                    iconImageId(R.id.iv_listitem_icon).build();
            adViewHolder.viewBinder = viewBinder;
            convertView.setTag(adViewHolder);
        } else {
            adViewHolder = (GroupAdViewHolder) convertView.getTag();
            viewBinder = adViewHolder.viewBinder;
        }
        bindData(convertView, adViewHolder, ad, viewBinder);
        if (ad.getImageList() != null && ad.getImageList().size() >= 3) {
            String image1 = ad.getImageList().get(0);
            String image2 = ad.getImageList().get(1);
            String image3 = ad.getImageList().get(2);
            if (image1 != null) {
                Glide.with(mContext).load(image1).into(adViewHolder.mGroupImage1);
            }
            if (image2 != null) {
                Glide.with(mContext).load(image2).into(adViewHolder.mGroupImage2);
            }
            if (image3 != null) {
                Glide.with(mContext).load(image3).into(adViewHolder.mGroupImage3);
            }
        }
        return convertView;
    }


    @SuppressWarnings("RedundantCast")
    private View getLargeAdView(View convertView, ViewGroup parent, @NonNull final TTNativeAd ad) {
        final LargeAdViewHolder adViewHolder;
        TTViewBinder viewBinder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.listitem_ad_large_pic, parent, false);
            adViewHolder = new LargeAdViewHolder();
            adViewHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_listitem_ad_title);
            adViewHolder.mDescription = (TextView) convertView.findViewById(R.id.tv_listitem_ad_desc);
            adViewHolder.mSource = (TextView) convertView.findViewById(R.id.tv_listitem_ad_source);
            adViewHolder.mLargeImage = (ImageView) convertView.findViewById(R.id.iv_listitem_image);
            adViewHolder.mIcon = (ImageView) convertView.findViewById(R.id.iv_listitem_icon);
            adViewHolder.mDislike = (ImageView) convertView.findViewById(R.id.iv_listitem_dislike);
            adViewHolder.mCreativeButton = (Button) convertView.findViewById(R.id.btn_listitem_creative);
            adViewHolder.mLogo = convertView.findViewById(R.id.tt_ad_logo);//logoView 建议传入GroupView类型

            adViewHolder.app_info = convertView.findViewById(R.id.app_info);
            adViewHolder.app_name = convertView.findViewById(R.id.app_name);
            adViewHolder.author_name = convertView.findViewById(R.id.author_name);
            adViewHolder.package_size = convertView.findViewById(R.id.package_size);
            adViewHolder.permissions_url = convertView.findViewById(R.id.permissions_url);
            adViewHolder.privacy_agreement = convertView.findViewById(R.id.privacy_agreement);
            adViewHolder.version_name = convertView.findViewById(R.id.version_name);

            viewBinder = new TTViewBinder.Builder(R.layout.listitem_ad_large_pic).
                    titleId(R.id.tv_listitem_ad_title).
                    decriptionTextId(R.id.tv_listitem_ad_desc).
                    sourceId(R.id.tv_listitem_ad_source).
                    mainImageId(R.id.iv_listitem_image).
                    callToActionId(R.id.btn_listitem_creative).
                    logoLayoutId(R.id.tt_ad_logo).//logoView 建议传入GroupView类型
                    iconImageId(R.id.iv_listitem_icon).build();
            adViewHolder.viewBinder = viewBinder;
            convertView.setTag(adViewHolder);
        } else {
            adViewHolder = (LargeAdViewHolder) convertView.getTag();
            viewBinder = adViewHolder.viewBinder;
        }
        bindData(convertView, adViewHolder, ad, viewBinder);
        if (ad.getImageUrl() != null) {
            Glide.with(mContext).load(ad.getImageUrl()).into(adViewHolder.mLargeImage);
        }
        return convertView;
    }

    @SuppressWarnings("RedundantCast")
    private View getSmallAdView(View convertView, ViewGroup parent, @NonNull final TTNativeAd ad) {
        SmallAdViewHolder adViewHolder;
        TTViewBinder viewBinder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.listitem_ad_small_pic, parent, false);
            adViewHolder = new SmallAdViewHolder();
            adViewHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_listitem_ad_title);
            adViewHolder.mSource = (TextView) convertView.findViewById(R.id.tv_listitem_ad_source);
            adViewHolder.mDescription = (TextView) convertView.findViewById(R.id.tv_listitem_ad_desc);
            adViewHolder.mSmallImage = (ImageView) convertView.findViewById(R.id.iv_listitem_image);
            adViewHolder.mIcon = (ImageView) convertView.findViewById(R.id.iv_listitem_icon);
            adViewHolder.mDislike = (ImageView) convertView.findViewById(R.id.iv_listitem_dislike);
            adViewHolder.mCreativeButton = (Button) convertView.findViewById(R.id.btn_listitem_creative);

            adViewHolder.app_info = convertView.findViewById(R.id.app_info);
            adViewHolder.app_name = convertView.findViewById(R.id.app_name);
            adViewHolder.author_name = convertView.findViewById(R.id.author_name);
            adViewHolder.package_size = convertView.findViewById(R.id.package_size);
            adViewHolder.permissions_url = convertView.findViewById(R.id.permissions_url);
            adViewHolder.privacy_agreement = convertView.findViewById(R.id.privacy_agreement);
            adViewHolder.version_name = convertView.findViewById(R.id.version_name);

            viewBinder = new TTViewBinder.Builder(R.layout.listitem_ad_small_pic).
                    titleId(R.id.tv_listitem_ad_title).
                    sourceId(R.id.tv_listitem_ad_source).
                    decriptionTextId(R.id.tv_listitem_ad_desc).
                    mainImageId(R.id.iv_listitem_image).
                    logoLayoutId(R.id.tt_ad_logo).//logoView 建议为GroupView 类型
                    callToActionId(R.id.btn_listitem_creative).
                    iconImageId(R.id.iv_listitem_icon).build();
            adViewHolder.viewBinder = viewBinder;
            convertView.setTag(adViewHolder);
        } else {
            adViewHolder = (SmallAdViewHolder) convertView.getTag();
            viewBinder = adViewHolder.viewBinder;
        }
        bindData(convertView, adViewHolder, ad, viewBinder);
        if (ad.getImageUrl() != null) {
            Glide.with(mContext).load(ad.getImageUrl()).into(adViewHolder.mSmallImage);
        }
        return convertView;
    }

    private static class VideoAdViewHolder extends AdViewHolder {
        FrameLayout videoView;
    }

    private static class LargeAdViewHolder extends AdViewHolder {
        ImageView mLargeImage;
    }

    private static class SmallAdViewHolder extends AdViewHolder {
        ImageView mSmallImage;
    }

    private static class VerticalAdViewHolder extends AdViewHolder {
        ImageView mVerticalImage;
    }

    private static class GroupAdViewHolder extends AdViewHolder {
        ImageView mGroupImage1;
        ImageView mGroupImage2;
        ImageView mGroupImage3;
    }

    private static class ExpressAdViewHolder {
        FrameLayout mAdContainerView;
    }

    private static class AdViewHolder {
        TTViewBinder viewBinder;
        ImageView mIcon;
        ImageView mDislike;
        Button mCreativeButton;
        TextView mTitle;
        TextView mDescription;
        TextView mSource;
        RelativeLayout mLogo;

        LinearLayout app_info;
        TextView app_name;
        TextView author_name;
        TextView package_size;
        TextView permissions_url;
        TextView privacy_agreement;
        TextView version_name;
    }
}
