package com.plugin.ad_mobrain.page;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bytedance.msdk.adapter.pangle.PangleNetworkRequestInfo;
import com.bytedance.msdk.api.AdError;
import com.bytedance.msdk.api.AdSlot;
import com.bytedance.msdk.api.NetworkPlatformConst;
import com.bytedance.msdk.api.TTAdConstant;
import com.bytedance.msdk.api.TTNetworkRequestInfo;
import com.bytedance.msdk.api.splash.TTSplashAd;
import com.bytedance.msdk.api.splash.TTSplashAdListener;
import com.bytedance.msdk.api.splash.TTSplashAdLoadCallback;
import com.gyf.immersionbar.ImmersionBar;
import com.plugin.ad_mobrain.R;
import com.plugin.ad_mobrain.utils.AdResult;
import com.plugin.ad_mobrain.utils.UIUtils;

import java.io.InputStream;

import io.flutter.embedding.engine.loader.FlutterLoader;

public class SplashActivity extends AppCompatActivity {
    private TTSplashAd mTTSplashAd;
    final private String TAG = "SplashAd";
    private String id = "";
    // 百度开屏广告点击跳转落地页后倒计时不暂停，即使在看落地页，倒计时结束后仍然会强制跳转，需要特殊处理：
    // 检测到广告被点击，且走了activity的onPaused证明跳转到了落地页，这时候onAdDismiss回调中不进行跳转，而是在activity的onResume中跳转。
    private boolean isBaiduSplashAd = false;
    private boolean baiduSplashAdClicked = false;
    private boolean onPaused = false;
    private String appId = "";
    private String adNetworkSlotId = "";
    private FrameLayout adsParent;
    private static final int AD_TIME_OUT = 4000;
    //是否强制跳转到主页面
    private boolean mForceGoMain;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBarScreen();
        setContentView(R.layout.activity_splash);
        Intent inten = getIntent();
        id = inten.getStringExtra("splashId");
        String splashFilePath = inten.getStringExtra("splashFilePath");
        appId = inten.getStringExtra("appId");
        adNetworkSlotId = inten.getStringExtra("adNetworkSlotId");
        ImageView imageView = this.findViewById(R.id.bottomIcon);
        imageView.setImageBitmap(AssetToBitmap(splashFilePath));
        adsParent = this.findViewById(R.id.adsFl);
        loadSplashAd();
    }

    /**
     * 加载开屏广告
     */
    private void loadSplashAd() {
        if (id == null || id.isEmpty()) return;
        /*
         * 注：每次加载开屏广告的时候需要新建一个TTSplashAd，否则可能会出现广告填充问题
         * （ 例如：mTTSplashAd = new TTSplashAd(this, mAdUnitId);）
         */
        mTTSplashAd = new TTSplashAd(this, id);
        mTTSplashAd.setTTAdSplashListener(mSplashAdListener);

        //step3:创建开屏广告请求参数AdSlot,具体参数含义参考文档
        AdSlot adSlot = new AdSlot.Builder()
                .setImageAdSize(UIUtils.getScreenWidth(this), UIUtils.getScreenHeight(this) - 90 - (int) UIUtils.getStatusBarHeight(this)) // 既适用于原生类型，也适用于模版类型。
                .setSplashButtonType(TTAdConstant.SPLASH_BUTTON_TYPE_FULL_SCREEN)
                .setDownloadType(TTAdConstant.DOWNLOAD_TYPE_POPUP)
                .build();

        //自定义兜底方案 选择使用
        TTNetworkRequestInfo ttNetworkRequestInfo;
        //穿山甲兜底，参数分别是appId和adn代码位。注意第二个参数是代码位，而不是广告位。
        ttNetworkRequestInfo = new PangleNetworkRequestInfo(appId, adNetworkSlotId);
        //gdt兜底
//        ttNetworkRequestInfo = new GdtNetworkRequestInfo(appId, adNetworkSlotId);
        //ks兜底
//        ttNetworkRequestInfo = new KsNetworkRequestInfo("90009", "4000000042");
        //百度兜底
//        ttNetworkRequestInfo = new BaiduNetworkRequestInfo("e866cfb0", "2058622");


        //step4:请求广告，调用开屏广告异步请求接口，对请求回调的广告作渲染处理
        mTTSplashAd.loadAd(adSlot, ttNetworkRequestInfo, new TTSplashAdLoadCallback() {
            @Override
            public void onSplashAdLoadFail(AdError adError) {
                AdResult.Nativate(AdResult.onAdFailed, "开屏广告:" + adError.message + "详情请根据code:(" + adError.code + ") 在官方文档查询", TAG, null, null);
                goToMainActivity();
            }

            @Override
            public void onSplashAdLoadSuccess() {
                if (mTTSplashAd != null) {
                    mTTSplashAd.showAd(adsParent);
                    isBaiduSplashAd = mTTSplashAd.getAdNetworkPlatformId() == NetworkPlatformConst.SDK_NAME_BAIDU;
//                    Logger.e(AppConst.TAG, "adNetworkPlatformId: " + mTTSplashAd.getAdNetworkPlatformId() + "   adNetworkRitId：" + mTTSplashAd.getAdNetworkRitId() + "   preEcpm: " + mTTSplashAd.getPreEcpm());
                    // 获取本次waterfall加载中，加载失败的adn错误信息。
//                    if (mTTSplashAd != null)
//                        Log.d(TAG, "ad load infos: " + mTTSplashAd.getAdLoadInfoList());
                }
                Log.e(TAG, "load splash ad success ");
            }

            @Override
            public void onAdLoadTimeout() {
                Log.i(TAG, "开屏广告加载超时.......");
                goToMainActivity();

            }
        }, AD_TIME_OUT);

    }

    TTSplashAdListener mSplashAdListener = new TTSplashAdListener() {
        @Override
        public void onAdClicked() {
            baiduSplashAdClicked = true;
            AdResult.Nativate(AdResult.onAdClick, "开屏广告click", TAG, null, null);
            Log.d(TAG, "onAdClicked");
        }

        @Override
        public void onAdShow() {
            AdResult.Nativate(AdResult.onAdShown, "开屏广告show", TAG, null, null);
            Log.d(TAG, "onAdShow");
        }

        /**
         * show失败回调。如果show时发现无可用广告（比如广告过期），会触发该回调。
         * 开发者应该结合自己的广告加载、展示流程，在该回调里进行重新加载。
         * @param adError showFail的具体原因
         */
        @Override
        public void onAdShowFail(AdError adError) {
//            showToast("开屏广告展示失败");
            Log.d(TAG, "onAdShowFail");

            // 开发者应该结合自己的广告加载、展示流程，在该回调里进行重新加载
            loadSplashAd();
        }

        @Override
        public void onAdSkip() {

//            showToast("开屏广告点击跳过按钮");
            Log.d(TAG, "onAdSkip");
            goToMainActivity();

//            goToMainActivity();
        }

        @Override
        public void onAdDismiss() {
//            showToast("开屏广告倒计时结束关闭");
            Log.d(TAG, "onAdDismiss");
            if (isBaiduSplashAd && onPaused && baiduSplashAdClicked) {
                // 这种情况下，百度开屏广告不能在onAdDismiss中跳转，需要在onResume中跳转主页。
                return;
            }
            goToMainActivity();
        }
    };

    private Bitmap AssetToBitmap(String assets) {
        Bitmap bitmap = null;
        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                InputStream inputStream = this.getApplicationContext().getAssets().open("flutter_assets/" + assets);
                bitmap = BitmapFactory.decodeStream(inputStream);
            } else {
                String assetLookupKey = FlutterLoader.getInstance().getLookupKeyForAsset(assets);
                AssetManager assetManager = this.getApplicationContext().getAssets();
                AssetFileDescriptor assetFileDescriptor = assetManager.openFd(assetLookupKey);
                InputStream inputStream = assetFileDescriptor.createInputStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mTTSplashAd != null) {
            mTTSplashAd.destroy();
        }
    }

    /**
     * 跳转到主页面
     */
    private void goToMainActivity() {
        AdResult.Nativate(AdResult.onAdClosed, "开屏广告close", TAG, null, null);
        adsParent.removeAllViews();
        this.finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mForceGoMain = true;
    }

    @Override
    protected void onResume() {
        if (mForceGoMain) {
            goToMainActivity();
        }
        if (isBaiduSplashAd && onPaused && baiduSplashAdClicked) {
            // 这种情况下，百度开屏广告不能在onAdDismiss中跳转，需要自己在onResume中跳转主页。
            goToMainActivity();
        }
        super.onResume();

    }

    public void setBarScreen() {
        ImmersionBar transparentStatusBar = ImmersionBar.with((Activity) this).fitsSystemWindows(false).transparentStatusBar();
        if (isBarDarkFont()) {
            transparentStatusBar.statusBarDarkFont(true, 0.2f);
        }
        transparentStatusBar.init();
    }

    public boolean isBarDarkFont() {
        return false;
    }
}
