package com.plugin.ad_mobrain.page;

import android.content.Context;

import androidx.annotation.NonNull;

import com.plugin.ad_mobrain.PluginDelegate;

import java.util.Map;

import io.flutter.plugin.common.StandardMessageCodec;
import io.flutter.plugin.platform.PlatformView;
import io.flutter.plugin.platform.PlatformViewFactory;

public class CustomNativeAdFactory  extends PlatformViewFactory {
    @NonNull
    private final PluginDelegate activity;

    public CustomNativeAdFactory(@NonNull PluginDelegate activity) {
        super(StandardMessageCodec.INSTANCE);
        this.activity = activity;
    }

    @Override
    public PlatformView create(Context context, int viewId, Object args) {
        final Map<String, Object> creationParams = (Map<String, Object>) args;
        return new CustomNativeAd(this.activity, creationParams);
    }
}
