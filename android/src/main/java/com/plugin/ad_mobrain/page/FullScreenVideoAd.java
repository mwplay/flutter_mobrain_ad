package com.plugin.ad_mobrain.page;

import android.util.Log;

import com.bytedance.msdk.api.AdError;
import com.bytedance.msdk.api.AdSlot;
import com.bytedance.msdk.api.GDTExtraOption;
import com.bytedance.msdk.api.TTAdConstant;
import com.bytedance.msdk.api.TTVideoOption;
import com.bytedance.msdk.api.fullVideo.TTFullVideoAd;
import com.bytedance.msdk.api.fullVideo.TTFullVideoAdListener;
import com.bytedance.msdk.api.fullVideo.TTFullVideoAdLoadCallback;
import com.plugin.ad_mobrain.PluginDelegate;
import com.plugin.ad_mobrain.utils.AdResult;
import com.plugin.ad_mobrain.utils.MainThreadResult;
import com.plugin.ad_mobrain.utils.VideoOptionUtil;

public class FullScreenVideoAd {
    final private String TAG = "FullScreenVideoAd";
    private PluginDelegate pluginDelegate;
    private String adId = "";
    private int template = 0;
    private String userId = "";
    private String extra = "";
    private String curReward = "";
    private int orientation = -1;
    private TTFullVideoAd mTTFullVideoAd;
    private MainThreadResult methodResult;

    public FullScreenVideoAd(String adId, int template, String userId, String extra, String curReward, int orientation, MainThreadResult result) {
        this.pluginDelegate = PluginDelegate.getInstance();
        this.adId = adId;
        this.template = template;
        this.userId = userId;
        this.extra = extra;
        this.curReward = curReward;
        this.orientation = orientation;
        methodResult = result;
    }

    public void loadAd() {
        int or = 1;
        if (orientation == 1) {
            or = TTAdConstant.VERTICAL;
        } else {
            or = TTAdConstant.HORIZONTAL;
        }

        /*
         * 注：每次加载全屏视频广告的时候需要新建一个TTFullVideoAd，否则可能会出现广告填充问题
         * （ 例如：mTTFullVideoAd = new TTFullVideoAd(this, adUnitId);）
         */
        mTTFullVideoAd = new TTFullVideoAd(pluginDelegate.activity, adId);

        //声音控制
        TTVideoOption videoOption = VideoOptionUtil.getTTVideoOption();

       if (template == GDTExtraOption.FeedExpressType.FEED_EXPRESS_TYPE_2) {
           videoOption = VideoOptionUtil.getTTVideoOption2();
       }
        //创建广告请求参数AdSlot,具体参数含义参考文档
        AdSlot.Builder adSlotBuilder = new AdSlot.Builder()
                .setTTVideoOption(videoOption)//设置声音控制
                .setUserID(userId)//用户id,必传参数
                .setMediaExtra(extra) //附加参数，可选
                .setOrientation(or);//必填参数，期望视频的播放方向：TTAdConstant.HORIZONTAL 或 TTAdConstant.VERTICAL;

        //请求广告
        mTTFullVideoAd.loadFullAd(adSlotBuilder.build(), new TTFullVideoAdLoadCallback() {

            @Override
            public void onFullVideoLoadFail(AdError adError) {
                methodResult.success(false);
                AdResult.Nativate(AdResult.onAdFailed, "全屏视频广告:" + adError.message + "详情请根据code:(" + adError.code + ") 在官方文档查询", TAG, curReward, null);
            }

            @Override
            public void onFullVideoAdLoad() {
                methodResult.success(true);
                AdResult.Nativate(AdResult.onAdLoaded, "全屏视频广告onload", TAG, curReward, null);
                mTTFullVideoAd.showFullAd(pluginDelegate.activity, mTTFullVideoAdListener);
            }

            @Override
            public void onFullVideoCached() {
                AdResult.Nativate(AdResult.onRewardVideoCached, "全屏视频广告cached", TAG, curReward, null);
            }
        });
    }


    /**
     * 激励视频交互回调
     */
    private TTFullVideoAdListener mTTFullVideoAdListener = new TTFullVideoAdListener() {

        @Override
        public void onFullVideoAdShow() {
//            TToast.show(FullVideoActivity.this, "全屏show");
            AdResult.Nativate(AdResult.onAdShown, "全屏视频广告show", TAG, curReward, null);
        }

        /**
         * show失败回调。如果show时发现无可用广告（比如广告过期或者isReady=false），会触发该回调。
         * 开发者应该结合自己的广告加载、展示流程，在该回调里进行重新加载。
         * @param adError showFail的具体原因
         */
        @Override
        public void onFullVideoAdShowFail(AdError adError) {

            // 开发者应该结合自己的广告加载、展示流程，在该回调里进行重新加载
            loadAd();
        }

        @Override
        public void onFullVideoAdClick() {
            AdResult.Nativate(AdResult.onAdClick, "全屏视频广告click", TAG, curReward, null);
        }

        @Override
        public void onFullVideoAdClosed() {
            AdResult.Nativate(AdResult.onAdClosed, "全屏视频广告close", TAG, curReward, null);
        }

        @Override
        public void onVideoComplete() {
//            TToast.show(FullVideoActivity.this, "全屏播放完成");
            Log.d(TAG, "onVideoComplete");
        }

        /**
         * 1、视频播放失败的回调 - Mintegral GDT Admob广告不存在该回调；
         */
        @Override
        public void onVideoError() {
            AdResult.Nativate(AdResult.onAdFailed, "全屏视频广告: 视频播放失败", TAG, curReward, null);
        }

        @Override
        public void onSkippedVideo() {
//            AdResult.Nativate(AdResult.onAdClosed, "全屏视频广告close", TAG, curReward, null);
        }
    };


    private void onDestroy() {
        if (mTTFullVideoAd != null) {
            mTTFullVideoAd.destroy();
        }
    }
}
