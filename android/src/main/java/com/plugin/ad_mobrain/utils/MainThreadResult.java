package com.plugin.ad_mobrain.utils;


import android.os.Handler;
import android.os.Looper;

import io.flutter.plugin.common.MethodChannel;

public class MainThreadResult implements MethodChannel.Result {
    private MethodChannel.Result result;
    private Handler handler;

    public MainThreadResult(MethodChannel.Result result) {
        this.result = result;
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void success(final Object o) {
        handler.post(
                new Runnable() {
                    @Override
                    public void run() {
                        result.success(o);
                    }
                });
    }

    @Override
    public void error(
            final String errorCode, final String errorMessage, final Object errorDetails) {
        handler.post(
                new Runnable() {
                    @Override
                    public void run() {
                        result.error(errorCode, errorMessage, errorDetails);
                    }
                });
    }

    @Override
    public void notImplemented() {
        handler.post(
                new Runnable() {
                    @Override
                    public void run() {
                        result.notImplemented();
                    }
                });
    }
}
