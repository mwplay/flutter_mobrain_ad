package com.plugin.ad_mobrain.utils;

import androidx.annotation.Nullable;

import com.plugin.ad_mobrain.PluginDelegate;

import java.util.HashMap;
import java.util.Map;

public class AdResult {
    public static String onAdLoaded = "1"; // 加载成功
    public static String onAdShown = "2"; // 显示
    public static String onAdClosed = "3"; // 广告关闭
    public static String onAdClick = "4"; // 广告点击
    public static String onAdFailed = "999"; // 广告发生错误
    /**
     * 当广告打开浮层时调用，如打开内置浏览器、内容展示浮层，一般发生在点击之后
     * 常常在onAdLeftApplication之前调用
     */
    public static String onAdOpened = "10"; // onAdOpened
    /**
     * 此方法会在用户点击打开其他应用（例如 Google Play）时
     * 于 onAdOpened() 之后调用，从而在后台运行当前应用。
     */
    public static String onAdLeftApp = "11";
    public static String onRewardVideoCached = "12"; // 激励或者全屏视频缓存成功

    public static void Nativate(String code, String msg, String TAG, @Nullable String curReward, @Nullable String nativeKey) {
        Map<String, Object> rewardVideoCallBack = new HashMap<>();
        String rew = "";
        if (curReward != null) {
            rew = curReward;
        }
        String natKey = "";
        if (nativeKey != null) {
            natKey = nativeKey;
        }
        rewardVideoCallBack.put("type", TAG);
        rewardVideoCallBack.put("callType", callFunc(code));
        rewardVideoCallBack.put("code", code);
        rewardVideoCallBack.put("msg", msg);
        rewardVideoCallBack.put("nativeKey", natKey);
        rewardVideoCallBack.put("rewardType", rew);
        PluginDelegate.getInstance().addEvent(rewardVideoCallBack);
    }

    private static String callFunc(String code) {
        String call = "";
        if (code.equals("1")) {
            call = "onAdLoaded";
        } else if (code.equals("2")) {
            call = "onAdShown";
        } else if (code.equals("3")) {
            call = "onAdClosed";
        } else if (code.equals("4")) {
            call = "onAdClick";
        } else if (code.equals("999")) {
            call = "onAdFailed";
        } else if (code.equals("10")) {
            call = "onAdOpened";
        } else if (code.equals("11")) {
            call = "onAdLeftApp";
        } else if (code.equals("12")) {
            call = "onRewardVideoCached";
        }
        return call;
    }
}
