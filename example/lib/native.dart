import 'package:ad_mobrain/page/NativeAd.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Native extends StatefulWidget {
  const Native({Key key}) : super(key: key);

  @override
  _NativeState createState() => _NativeState();
}

class _NativeState extends State<Native> {
  List<NativeAd> _adList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // _eventHandler = AdscopePlugin.adScopeEventHandler.listen((event) {
    //   print("NativeAd====>" +
    //       event.callType +
    //       '=====>' +
    //       event.msg +
    //       '=====>' +
    //       event.code);
    // });
  }

  void _addNative() {
    setState(() {
      _adList.add(_adView());
    });
  }

  NativeAd _adView() {
    return new NativeAd(
      width: 1280.0.w,
      height: 720.0.w,
      id: "${_adList.length + 1}",
      delFunc: delFunction,
    );
  }

  void delFunction(String id) {
    print("删掉的id==>" + id);
    setState(() {
      _adList = _adList.where((element) => element.id != id).toList();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 60.0.w,
          ),
          MaterialButton(
            onPressed: _addNative,
            child: Container(
              width: 400.0.w,
              height: 60.0.w,
              decoration: BoxDecoration(
                color: Colors.grey,
              ),
              child: Center(
                child: Text(
                  "加载广告",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          Container(
            height: 25.0.w,
          ),
          Container(
            height: Get.height - 85.0.w - 100.0.w,
            child: ListView.builder(
                itemCount: _adList.length,
                itemBuilder: (b, int s) {
                  return _adList[s];
                }),
          ),
        ],
      ),
    );
  }
}
