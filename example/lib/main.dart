import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:ad_mobrain/ad_mobrain.dart';
import 'package:flutter_screenutil/screenutil_init.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'banner.dart';
import 'native.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'reward.dart';

void main() {
  runApp(MyAppHome());
}

class MyAppHome extends StatefulWidget {
  const MyAppHome({Key key}) : super(key: key);

  @override
  _MyAppHomeState createState() => _MyAppHomeState();
}

class _MyAppHomeState extends State<MyAppHome> {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(750, 1334),
      allowFontScaling: true,
      builder: () => GetMaterialApp(
        debugShowCheckedModeBanner: true,
        title: 'adscope',
        home: MyApp(),
      ),
    );
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String oaid = "";
  String imei = "";
  AdMobrainConfig _adConfig = AdMobrainConfig(
    appId: "5148567",
    // appId
    appName: "广告测试",
    // app name
    interstitialId: "946702312",
    // 插屏广告idH
    openDebug: true,
    rewardVideoId: "946702313",
    // 激励视频id
    bannerHeight: 120.0,
    // banner高度  可以在使用的时候传 这里就不用传了
    bannerId: "946702310",
    // bannerId
    nativeId: "946702311",
    // 信息流id
    splashId: "887565081",
    // 开屏id
    splashBottomImgFilePath: "assets/images/img_splash2.png",
    // 开屏bottom 图片
    fullScreenVideoVerticalId: "946706687", // 全屏视频id
  );

  @override
  void initState() {
    super.initState();
    initAd();
  }

  //初始化广告
  void initAd() async {
    AdMobrainPlugin.adMobrainEventHandler.listen((value) {
      print(value.toString());
    });
    await [
      Permission.location,
      Permission.storage,
      Permission.phone,
    ].request();
    final bool result = await AdMobrainPlugin.initAd(_adConfig);
    await _showSplashAd();
    // setState(() async {
    //   oaid = await AdMobrainPlugin.getOaid();
    //   imei = await AdMobrainPlugin.getImei();
    // });
    // print(result);
  }

  //显示插屏广告
  void _showInterstitialIdAd() async {
    AdMobrainPlugin.showInterstitialIdAd();
  }

  void _showRewardAd() async {
    AdMobrainPlugin.showRewardVideoAd(extra: "12", userId: "123");
  }

  void _getOaid() async {
    await AdMobrainPlugin.getOaid().then((value) {
      setState(() {
        oaid = "$value";
      });
    });
  }

  void _getImei() async {
    await AdMobrainPlugin.getImei().then((value) {
      setState(() {
        imei = "$value";
      });
    });
  }

  // 显示底部广告
  void _showBannerAd() {
    Get.to(
      () => BannerAd(),
      opaque: false,
      fullscreenDialog: true,
      duration: 0.milliseconds,
      transition: Transition.noTransition,
    );
  }

  //显示信息流
  void _showNativeAd() {
    Get.to(
      () => Native(),
      opaque: false,
      fullscreenDialog: true,
      duration: 0.milliseconds,
      transition: Transition.noTransition,
    );
  }

  // 显示全屏视频
  void _showFullScreenVideoAd() async {
    await AdMobrainPlugin.showFullScreenVideoAd(rewardType: 1, userId: "1");
  }

  // 加载开屏
  void _showSplashAd() async {
    await AdMobrainPlugin.showSplashAd();
  }

  void _goRewardLoad() async {
    Get.to(
      () => Reward(),
      opaque: false,
      fullscreenDialog: true,
      duration: 0.milliseconds,
      transition: Transition.noTransition,
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: [
              Container(
                width: Get.width,
                child: Text(
                  "oaid: ${oaid}",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 36.0.w,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                width: Get.width,
                child: Text(
                  "imei: ${imei}",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 36.0.w,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              MaterialButton(
                onPressed: _showInterstitialIdAd,
                child: Text("打开插屏广告"),
              ),
              MaterialButton(
                onPressed: _showRewardAd,
                child: Text("打开激励广告"),
              ),
              MaterialButton(
                onPressed: _showNativeAd,
                child: Text("原生信息流广告"),
              ),
              MaterialButton(
                onPressed: _showBannerAd,
                child: Text("点击去看底部banner"),
              ),
              MaterialButton(
                onPressed: _getOaid,
                child: Text("获取oaid"),
              ),
              MaterialButton(
                onPressed: _getImei,
                child: Text("获取imei"),
              ),
              MaterialButton(
                onPressed: _showFullScreenVideoAd,
                child: Text("播放全屏视频广告"),
              ),
              MaterialButton(
                onPressed: _showSplashAd,
                child: Text("加载开屏广告"),
              ),
              MaterialButton(
                onPressed: _goRewardLoad,
                child: Text("前往预加载激励视频部分"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
