import 'package:ad_mobrain/ad_mobrain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Reward extends StatefulWidget {
  const Reward({Key key}) : super(key: key);

  @override
  _RewardState createState() => _RewardState();
}

class _RewardState extends State<Reward> {
  var _listen;
  String _state = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _listen = AdMobrainPlugin.adMobrainEventHandler.listen((value) {
      if (value.type == AdmobrainType.rewardedVideoAd) {
        print("激励广告监听==>" + value.toString());
        if (value.code == '12') {
          //  视频缓存成功
          setState(() {
            _state = value.msg;
          });
        }
      }
    });
  }

  void _loadAd() async {
    await AdMobrainPlugin.showRewardVideoAd(
      scenario: 1,  // 预加载必传  普通播放不用传
      rewardType: 11, //  不使用可不传
      userId: '1',
      extra: '1', // 扩展参数 不使用可以不传
    );
  }

  void _playAd() async {
    await AdMobrainPlugin.showRewardVideoAd(
      scenario: 1, // 场景  必传
      rewardType: 11, // 当前播放广告下标  不使用可不传
      isPlay: true, // 播放广告  必传
    );
  }

  void _getAdStatus() async {
    final bool s = await AdMobrainPlugin.getRewardStatus();
    setState(() {
      if (s) {
        setState(() {
          _state = "广告已就绪";
        });
      } else {
        setState(() {
          _state = "广告未加载";
        });
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _listen.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 100.0.w,
          ),
          Text(_state),
          MaterialButton(
            onPressed: _loadAd,
            child: Text("加载广告"),
          ),
          MaterialButton(
            onPressed: _playAd,
            child: Text("播放广告"),
          ),
          MaterialButton(
            onPressed: _getAdStatus,
            child: Text("查询广告状态"),
          ),
        ],
      ),
    );
  }
}
