import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ad_mobrain/ad_mobrain.dart';

void main() {
  const MethodChannel channel = MethodChannel('ad_mobrain');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
  });
}
