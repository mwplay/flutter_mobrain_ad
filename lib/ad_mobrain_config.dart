class AdMobrainConfig {
  String _app_id = ''; // 必传id
  String _app_name = ''; // 必传name
  bool _open_debug = false; // 是否打开日志  非必传 默认false

  // 使用TextureView控件播放视频,默认为SurfaceView,当有SurfaceView冲突的场景，可以使用TextureView
  bool _use_pangle_texture_view = true; //  非必传  默认为true

  bool _allow_pangle_show_notify = true; //是否允许sdk展示通知栏提示 非必传默认为true

  // 插屏广告Id
  String _interstitial_id = '';

  // 底部广告id
  String _banner_id = '';

  // 激励广告id
  String _reward_video_id = '';

  // 开屏广告id
  String _splash_id = '';

  // 开屏广告底部图片path
  String _splash_bottom_img_file_path = "";

  // 信息流nativeId
  String _native_id= '';

  // banner高度
  double _banner_height = 0;

  // 全屏视频加载 竖屏
  String _full_screen_video_vertical_id = '';

  // 开屏兜底
  String _splashNetworkSlotId = '';

  AdMobrainConfig({
    required String appId,
    required String appName,
    bool openDebug = false,
    String interstitialId = '',
    String bannerId = '',
    String rewardVideoId = '',
    String splashId = '',
    String splashBottomImgFilePath= '',
    String nativeId = '',
    double bannerHeight = 0,
    String fullScreenVideoVerticalId = '',
    String splashNetworkSlotId = '',
  }) {
    this._app_id = appId;
    this._app_name = appName;
    this._open_debug = openDebug;
    this._interstitial_id = interstitialId;
    this._native_id = nativeId;
    this._banner_id = bannerId;
    this._splash_id = splashId;
    this._splash_bottom_img_file_path = splashBottomImgFilePath;
    this._reward_video_id = rewardVideoId;
    this._banner_height = bannerHeight;
    this._full_screen_video_vertical_id = fullScreenVideoVerticalId;
    this._splashNetworkSlotId = splashNetworkSlotId;
  }

  String get splashNetworkSlotId => _splashNetworkSlotId;

  String get interstitial_id => _interstitial_id;

  set interstitial_id(String value) {
    _interstitial_id = value;
  }

  String get app_id => _app_id;

  set app_id(String value) {
    _app_id = value;
  }

  bool get allow_pangle_show_notify => _allow_pangle_show_notify;

  set allow_pangle_show_notify(bool value) {
    _allow_pangle_show_notify = value;
  }

  bool get use_pangle_texture_view => _use_pangle_texture_view;

  set use_pangle_texture_view(bool value) {
    _use_pangle_texture_view = value;
  }

  bool get open_debug => _open_debug;

  set open_debug(bool value) {
    _open_debug = value;
  }

  String get app_name => _app_name;

  set app_name(String value) {
    _app_name = value;
  }

  String get banner_id => _banner_id;

  set banner_id(String value) {
    _banner_id = value;
  }

  String get reward_video_id => _reward_video_id;

  String get native_id => _native_id;

  set native_id(String value) {
    _native_id = value;
  }

  String get splash_bottom_img_file_path => _splash_bottom_img_file_path;

  set splash_bottom_img_file_path(String value) {
    _splash_bottom_img_file_path = value;
  }

  String get splash_id => _splash_id;

  set splash_id(String value) {
    _splash_id = value;
  }

  set reward_video_id(String value) {
    _reward_video_id = value;
  }

  double get banner_height => _banner_height;

  set banner_height(double value) {
    _banner_height = value;
  }

  String get full_screen_video_vertical_id => _full_screen_video_vertical_id;

  set full_screen_video_vertical_id(String value) {
    _full_screen_video_vertical_id = value;
  }
}
