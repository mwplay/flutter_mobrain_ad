import 'dart:async';
import 'dart:collection';

import 'package:ad_mobrain/ad_mobrain_config.dart';
import 'package:flutter/services.dart';

import 'AdMobrainRespon.dart';

class AdMobrainPlugin {
  // 方法通道
  static const MethodChannel _methodChannel = const MethodChannel('ad_mobrain');

  // 事件通道
  static const EventChannel _eventChannel =
      const EventChannel('ad_mobrain_event');

  static StreamController<AdMobrainRespon>
      _adMobrainResponseEventHandlerController =
      new StreamController.broadcast();

  static Stream<AdMobrainRespon> get adMobrainEventHandler =>
      _adMobrainResponseEventHandlerController.stream;

  static late AdMobrainConfig _adMobrainConfig;

  // 把配置文件透露出去
  static AdMobrainConfig get adMobrainConfig => _adMobrainConfig;

  // 初始化监听
  static void _fetchEvent() {
    _eventChannel.receiveBroadcastStream().listen((dynamic a) {
      String rewardType = a['rewardType'] ?? "";
      int re = -1;
      if (rewardType != null && rewardType.isNotEmpty) {
        re = int.parse(rewardType);
      }
      String nativeKey = a['nativeKey'] ?? "";
      AdMobrainRespon respon = new AdMobrainRespon(
        type: a['type'],
        callType: a['callType'],
        code: a['code'],
        msg: a['msg'],
        rewardType: re,
        nativeKey: nativeKey,
      );
      _adMobrainResponseEventHandlerController.add(respon);
    });
  }

  /// 初始化广告
  /// [appId] 应用媒体ID
  static Future<bool> initAd(AdMobrainConfig config) async {
    _fetchEvent();

    Map<String, dynamic> map = new HashMap();
    map['appId'] = config.app_id;
    map['appName'] = config.app_name;
    map['usePangleTextureView'] = config.use_pangle_texture_view;
    map['allowPangleShowNotify'] = config.allow_pangle_show_notify;
    map['openDebug'] = config.open_debug;
    _adMobrainConfig = config;
    final bool result = await _methodChannel.invokeMethod('initAd', map);
    return result;
  }

  //  显示插屏广告
  static Future<bool> showInterstitialIdAd() async {
    if (_adMobrainConfig.interstitial_id.isEmpty) {
      AdMobrainRespon respon = new AdMobrainRespon(
        type: 'InterstitialAd',
        callType: 'onAdFailed',
        code: '999',
        msg: '请在初始化的时候传入插屏广告ID (interstitial_id)',
        rewardType: -1,
        nativeKey: "",
      );
      _adMobrainResponseEventHandlerController.add(respon);
      return false;
    }

    Map _arg = Map();
    _arg['InterstitialId'] = _adMobrainConfig.interstitial_id;
    final bool result =
        await _methodChannel.invokeMethod('InterstitialAd', _arg);
    return result;
  }

  //显示激励广告
  static Future<bool?> showRewardVideoAd({
    String extra = "", // 扩展参数
    int rewardType = -1,
    String userId = "",
    String rewardName = "奖励", //奖励名称
    int rewardAmount = 0, // 奖励数量
    int scenario = 999,
    bool isPlay = false, // 此选项为true的时候代表播放已加载的广告
  }) async {
    if (_adMobrainConfig.reward_video_id.isEmpty) {
      AdMobrainRespon respon = new AdMobrainRespon(
        type: 'RewardedVideoAd',
        callType: 'onAdFailed',
        code: '999',
        msg: '请在初始化的时候传入激励广告ID (reward_video_id)',
        rewardType: -1,
        nativeKey: "",
      );
      _adMobrainResponseEventHandlerController.add(respon);
      return false;
    }
    Map _arg = Map();
    _arg['RewardVideoId'] = _adMobrainConfig.reward_video_id;
    _arg['RewardVideoExtra'] = extra;
    _arg['RewardVideoRewardType'] = "$rewardType";
    _arg['userId'] = userId;
    _arg['rewardName'] = rewardName;
    _arg['rewardAmount'] = rewardAmount;
    _arg['scenario'] = scenario;
    _arg['isPlay'] = isPlay;
    final bool re = await _methodChannel.invokeMethod('RewardVideoAd', _arg);
    return re;
  }

  // 开屏广告
  static Future<String?> showSplashAd() async {
    if (_adMobrainConfig.splash_id.isEmpty ||
        _adMobrainConfig.splash_bottom_img_file_path.isEmpty ||
        _adMobrainConfig.splashNetworkSlotId.isEmpty) {
      AdMobrainRespon respon = new AdMobrainRespon(
        type: 'splashAd',
        callType: 'onAdFailed',
        code: '999',
        msg: '请在初始化的时候传入开屏广告ID (splash_id)',
        rewardType: -1,
        nativeKey: "",
      );
      if (_adMobrainConfig.splash_bottom_img_file_path.isEmpty) {
        respon.setMsg = "请传入开屏广告底部图片地址";
      }
      if (_adMobrainConfig.splashNetworkSlotId.isEmpty) {
        respon.setMsg = "请在初始化的时候传入开屏兜底Id(splashNetworkSlotId)";
      }
      _adMobrainResponseEventHandlerController.add(respon);
      return "";
    }
    Map _arg = Map();
    _arg['SplashId'] = _adMobrainConfig.splash_id;
    _arg['splashFilePath'] = _adMobrainConfig.splash_bottom_img_file_path;
    _arg['appId'] = _adMobrainConfig.app_id;
    _arg['adNetworkSlotId'] = _adMobrainConfig.splashNetworkSlotId;
    await _methodChannel.invokeMethod('splashAd', _arg).then((value) {
      Future.value(value);
    }).catchError((err) {
      Future.error(err);
    });
  }

  //加载全屏视频
  static Future<bool?> showFullScreenVideoAd({
    int template = 1, // 默认1 不动就行
    int rewardType = -1,
    String userId = '', // userId
    String extra = '', // 扩展参数
    int orientation = 1, // 广告加载方向 暂时只支持竖屏 1. 竖屏 2.横屏  默认 1
  }) async {
    if (_adMobrainConfig.full_screen_video_vertical_id.isEmpty) {
      AdMobrainRespon respon = new AdMobrainRespon(
        type: 'RewardedVideoAd',
        callType: 'onAdFailed',
        code: '999',
        msg: '请在初始化的时候传入全屏视频广告ID (full_screen_video_vertical_id)',
        rewardType: -1,
        nativeKey: "",
      );
      _adMobrainResponseEventHandlerController.add(respon);
      return false;
    }
    Map _arg = Map();
    _arg['fullScreenVideoVerticalId'] =
        _adMobrainConfig.full_screen_video_vertical_id;
    _arg['template'] = template;
    _arg['userId'] = userId;
    _arg['extra'] = extra;
    _arg['orientation'] = orientation;
    _arg['curReward'] = "$rewardType";
    final bool re =
        await _methodChannel.invokeMethod('fullScreenVideoAd', _arg);
    return re;
  }

  static Future<bool?> getRewardStatus() async {
    final bool status = await _methodChannel.invokeMethod('getRewardStatus');
    return status;
  }

  // 获取oaid
  static Future<String?> getOaid() async {
    final String oaid = await _methodChannel.invokeMethod('oaid');
    return oaid;
  }

  // 获取imei
  static Future<String?> getImei() async {
    final String imei = await _methodChannel.invokeMethod('imei');
    return imei;
  }
}
