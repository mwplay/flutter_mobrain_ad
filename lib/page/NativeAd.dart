/*
 * @Author: your name
 * @Date: 2021-08-10 18:33:58
 * @LastEditTime: 2021-08-10 18:34:17
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \cgwzx\lib\pages\pop\bottom_banner_ad.dart
 */
import 'dart:math';

import 'package:ad_mobrain/ad_mobrain.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../AdMobrainRespon.dart';

typedef void del(String id);

void dell(String s) {}

class NativeAd extends StatefulWidget {
  final bool show; // 是否显示广告
  final double height; // 广告高度 不传的话默认屏幕宽度
  final double width; // 广告宽度
  final bool isWhiteBackGround; // 是否需要显示一个白色背景
  final int time; // 广告超时时长  默认5000ms
  final int template; // （1表示平台模板为1.0；2表示平台模板2.0；3表示自渲染） 默认1
  final String id; // 随机id
  final del delFunc;
  final int adCount;
  final int mAdStyle; // 1.模板广告  2. 原生广告 默认1

  NativeAd({
    this.show = true,
    this.width = 0.0,
    this.height = 0.0,
    this.isWhiteBackGround = true,
    this.time = 5000,
    this.template = 1,
    this.id = "",
    this.delFunc = dell,
    this.adCount = 1,
    this.mAdStyle = 1,
  });

  @override
  _NativeAdState createState() => _NativeAdState();
}

class _NativeAdState extends State<NativeAd> {
  var _eventHandler;
  String _wlKey = "";

  // final size = MediaQuery.of(context).size;
  Widget _adElem(double w) {
    double wl = widget.width > 0 ? widget.width : w;
    double hl = widget.height;
    if (!widget.show) {
      return Container(
        height: 0.0,
      );
    }

    if (hl == 0) {
      return errContro("请在使用的时候传入高度,才能正常使用", wl);
    }
    if (AdMobrainPlugin.adMobrainConfig.native_id.isEmpty) {
      return errContro("请在初始化的时候传入 native_id", wl);
    }

    return Container(
      height: hl,
      child: Stack(
        children: [
          Visibility(
            visible: widget.isWhiteBackGround,
            child: Container(
              width: wl,
              height: hl,
              color: Colors.red,
            ),
          ),
          AndroidView(
            viewType: 'com.mobrain.native',
            creationParams: {
              'width': bannerAdSize(wl), // 宽度
              'height': bannerAdSize(hl), // 高度
              'nativeId': AdMobrainPlugin.adMobrainConfig.native_id, // 广告位id
              'adCount': widget.adCount,
              'mAdStyle': widget.mAdStyle,
              "nativeKey": _wlKey,
            },
            creationParamsCodec: const StandardMessageCodec(),
          ),
        ],
      ),
    );
  }

  Widget errContro(String msg, double wl) {
    return Container(
      width: wl,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Text(
        msg,
        textAlign: TextAlign.center,
      ),
    );
  }

  // 转换为android的尺寸
  int bannerAdSize(double adSize) {
    print(adSize);
    MediaQueryData queryData = MediaQuery.of(context);
    return (adSize * queryData.devicePixelRatio).round();
  }

  @override
  void initState() {
    _eventHandler = AdMobrainPlugin.adMobrainEventHandler.listen((event) {
      if (event.type == AdmobrainType.nativeAd && event.nativeKey == _wlKey) {
        if (event.code == '3' || event.code == '999') {
          if (widget.delFunc != null) {
            widget.delFunc(_wlKey);
          }
        }
      }
    });
    if (widget.id == null || widget.id.isEmpty) {
      _wlKey = _randomString();
    } else {
      _wlKey = widget.id;
    }
    super.initState();
  }

  @override
  void dispose() {
    _eventHandler.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return _adElem(size.width);
  }

  String _randomString() {
    String alphabet =
        'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM123456789';
    int strlenght = 30;

    /// 生成的字符串固定长度
    String left = '';
    for (var i = 0; i < strlenght; i++) {
      left = left + alphabet[Random().nextInt(alphabet.length)];
    }
    return left;
  }
}
