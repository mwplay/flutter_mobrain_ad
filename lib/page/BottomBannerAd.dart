/*
 * @Author: your name
 * @Date: 2021-08-10 18:33:58
 * @LastEditTime: 2021-08-10 18:34:17
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \cgwzx\lib\pages\pop\bottom_banner_ad.dart
 */
import 'package:ad_mobrain/ad_mobrain.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BottomBannerAd extends StatefulWidget {
  final bool show; // 是否显示广告
  final double height; // 广告高度
  final double width; // 广告宽度
  final bool isWhiteBackGround; // 是否需要显示一个白色背景

  BottomBannerAd({
    this.show = true,
    this.width = 0.0,
    this.height = 0.0,
    this.isWhiteBackGround = true,
  });

  @override
  _BottomBannerAdState createState() => _BottomBannerAdState();
}

class _BottomBannerAdState extends State<BottomBannerAd> {
  // final size = MediaQuery.of(context).size;
  Widget _adElem(double w) {
    double wl = widget.width > 0 ? widget.width : w;
    double hl = widget.height > 0
        ? widget.height
        : AdMobrainPlugin.adMobrainConfig.banner_height;
    // print("$wl====$hl---${widget.show}");
    if (!widget.show) {
      return Container(
        height: 0.0,
      );
    }

    if (hl == 0) {
      return errContro("请在初始化或者使用的时候传入banner高度,才能正常使用", wl);
    }
    if (AdMobrainPlugin.adMobrainConfig.banner_id.isEmpty) {
      return errContro("请在初始化的时候传入banner_Id", wl);
    }

    return Container(
      height: hl,
      child: Stack(
        children: [
          Visibility(
            visible: widget.isWhiteBackGround,
            child: Container(
              width: wl,
              height: hl,
              color: Colors.white,
            ),
          ),
          AndroidView(
            viewType: 'com.mobrain.banner',
            creationParams: {
              'width': wl.round(), // 宽度
              'height': hl.round(), // 高度
              'bannerId': AdMobrainPlugin.adMobrainConfig.banner_id, // 广告位id
            },
            creationParamsCodec: const StandardMessageCodec(),
          ),
        ],
      ),
    );
  }

  Widget errContro(String msg, double wl) {
    return Container(
      width: wl,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Text(
        msg,
        textAlign: TextAlign.center,
      ),
    );
  }

  // 转换为android的尺寸
  int bannerAdSize(double adSize) {
    MediaQueryData queryData = MediaQuery.of(context);
    return (adSize * queryData.devicePixelRatio).round();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return _adElem(size.width);
  }
}
