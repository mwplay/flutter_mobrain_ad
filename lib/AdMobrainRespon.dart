class AdMobrainRespon {
// ignore: unused_field
  String _callType = "";

  // ignore: unused_field
  String _msg = "";

  // ignore: unused_field
  String _code = "";

  // ignore: unused_field
  late int _type;

  //激励广告当前播放对象
  int _rewardType = -1;

  // 信息流当前操作最想
  String _nativeKey = "";

  AdMobrainRespon({
    required String callType,
    required String msg,
    required String code,
    required String type,
    int rewardType = -1,
    String nativeKey = "",
  }) {
    this._callType = callType;
    this._msg = msg;
    this._code = code;
    this._type = _switchType(type);
    this._rewardType = rewardType;
    this._nativeKey = nativeKey;
  }

  // 返回类型
  int _switchType(String type) {
    switch (type) {
      case "init":
        return AdmobrainType.init;
      case "InterstitialAd":
        return AdmobrainType.interstitialAd;
      case "BannerAd":
        return AdmobrainType.bannerAd;
      case "RewardedVideoAd":
        return AdmobrainType.rewardedVideoAd;
      case "SplashAd":
        return AdmobrainType.splashAd;
      case "NativeAd":
        return AdmobrainType.nativeAd;
      case "FullScreenVideoAd":
        return AdmobrainType.FullScreenVideoAd;
      default:
        return -1;
    }
  }

  String get callType => _callType;

  int get type => _type;

  String get code => _code;

  String get msg => _msg;

  int get rewardType => _rewardType;

  String get nativeKey => _nativeKey;

  set setMsg(String value) {
    _msg = value;
  }

  @override
  String toString() {
    return 'AdMobrainRespon{_callType: $_callType, _msg: $_msg, _code: $_code, _type: $_type, _rewardType: $_rewardType, _nativeKey: $_nativeKey}';
  }
}

class AdmobrainType {
  // 初始化
  static const int init = 0;

  // 插屏
  static const int interstitialAd = 1;

  //  底部banner广告
  static const int bannerAd = 2;

  //  激励
  static const int rewardedVideoAd = 3;

  // 开屏广告
  static const int splashAd = 4;

  // 信息流native
  static const int nativeAd = 5;

  // 全屏视频
  static const int FullScreenVideoAd = 6;
}
